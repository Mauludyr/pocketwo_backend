<?php

namespace App\Providers;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\ServiceProvider;

class AddonsServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        try {
            if (!empty($addons = available_modules('addon'))) {
                foreach ($addons as $addon) {
                    if (file_exists($provider = base_path(implode(DIRECTORY_SEPARATOR, ['addons',$addon, 'Provider', 'RouteServiceProvider.php'])))) {
                        $this->app->register("\Addons\\$addon\Provider\RouteServiceProvider");
                    }

                    if (file_exists($views = base_path(implode(DIRECTORY_SEPARATOR, ['addons', $addon, 'Views'])))) {
                        $this->loadViewsFrom($views, $addon);
                    }

                    if (file_exists($config = base_path(implode(DIRECTORY_SEPARATOR, ['addons', $addon, 'Config', 'addon.php'])))) {
                        $this->mergeConfigFrom($config, 'modules');
                    }

                    if (file_exists($migrations = base_path(implode(DIRECTORY_SEPARATOR, ['addons', $addon, 'Database', 'migrations'])))) {
                        $this->loadMigrationsFrom($migrations);
                    }

                    if (class_exists($addonLoader = "\\Addons\\{$addon}\\{$addon}")) {
                        $this->app->bind(strtolower($addon), function () use ($addonLoader) {
                            return new $addonLoader();
                        });
                    }

                    if (file_exists($eventProvider = base_path(implode(DIRECTORY_SEPARATOR, ['addons',$addon, 'Provider', 'EventServiceProvider.php'])))) {
                        $this->app->register("\Addons\\$addon\Provider\EventServiceProvider");
                    }
                }
            }
        } catch (\Exception $e) {
            if (env('APP_DEBUG', false)) {
                save_error_log($e, 'addon-service');
            }
        }
    }
}
