<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\User;
use App\Mail\SystemEmail;
use Illuminate\Support\Facades\Mail;

class KYCReminder extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'kyc:reminder';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'KYC Reminder Daily';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $datauser = User::with('kyc_applicants','kyc_docs','kyc_sessions')->get();
        foreach ($datauser as $key => $value) {
            if (count($value->kyc_applicants) == 0 || count($value->kyc_docs) == 0 || count($value->kyc_sessions) == 0){
                try {
                     $data = [
                        "subject" => 'Dont Forget To Update Your KYC',
                        "name" => $value->name,
                        "message" => 'Dont Forget To Update Your KYC'
                    ];
                    Mail::to($value->email)->send(new SystemEmail($data, 'users.custom-email-kyc-reminder'));
                    $this->info("Send Email Successfully.");
                } catch (\Exception $e) {
                    $this->error($e);
                }
            }
        }
        // return 0;
    }
}
