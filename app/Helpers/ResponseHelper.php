<?php

namespace App\Helpers;

class ResponseHelper{

    public function errorCustom($code,$msg)
    {
    	$response = array(
            'code'=>$code,
            'message'=>$msg,
            'error'=>true
        );
    	return $response;
    }
    public function successWithData($input) 
    {
        $response = array(
            'code' => 200,
            'message' => 'Success',
            'error' => false,
            'data' => $input
        );
    	return $response;
    }
    public function errorWithData($code,$msg,$input)
    {
        $response = array(
            'code' => $code,
            'message' => $msg,
            'error' => true,
            'data' => $input
        );
        return $response;
    }   
    public function successWithoutData($msg)
    {
        $response = array(
            'code' => 200,
            'message' => $msg,
            'error' => false
        );
    	return $response;
    }
    public function errorValidation($code,$msg, $err)
    {
    	$response = array(
    	    'code'=>$code,
            'message'=>$msg,
            'error'=>true,
            'errors' => $err
    	);
    	return $response;
    }
}
