<?php


namespace App\Enums;


interface AccountBalanceType
{
    const MAIN = 'main_wallet';
    const REFERRAL = 'referral_account';
    const MAIN_HOLD = 'main_wallet_hold';
}
