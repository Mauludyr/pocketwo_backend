<?php

namespace App\Http\Controllers;

use App\Enums\UserRoles;
use App\Enums\SchemeStatus;

use App\Models\User;
use App\Services\Shortcut;
use App\Services\MaintenanceService as MService;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Validation\ValidationException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Str;

class PublicController extends Controller
{
	public function __construct()
    {
 
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function welcome()
    {
    	if (gss('front_page_enable', 'yes')=='no') {
            return redirect()->route('auth.register.form');
        }

        $logged     = Auth::check();
        $admins     = ($logged && in_array(Auth::user()->role, [UserRoles::ADMIN, UserRoles::SUPER_ADMIN]));

        return view('frontend.index', compact('admins'));
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function language(Request $request)
    {
        $lang = $request->get('lang', gss('language_default_public', 'en'));

        $setlocale = Cookie::queue(Cookie::make('app_language', $lang, (60 * 24 * 365)));
        
        return back();
    }

    public function gdprCookie(Request $request)
    {
        try {
            $request->validate(['consent' => 'required|string|in:yes,no']);
            $name = '_pconsent_' . ghp();

            if ($request->consent == 'yes') {
                $minutes = 2 * 365 * 24 * 60;
                $cookie = cookie($name, 'yes', $minutes);
                return response()->json(['status' => 'success', 'message' => __("Cookie Accepted.")])->withCookie($cookie);
            }
    
            if ($request->consent == 'no') {
                $minutes = 30 * 24 * 60;
                $cookie = cookie($name, 'no', $minutes);
                return response()->json(['status' => 'success', 'message' => __("Cookie Denied.")])->withCookie($cookie);
            }
        } catch (\Exception $e) {
            return response()->json(['status' => 'error', 'message' => __("Please reload the page and try again.")]);
        }
    }
}
