<?php


namespace App\Http\Controllers\Admin;

use App\Models\User;
use App\Models\LoanRequest;
use App\Models\LoanOffer;
use App\Models\LoanTransaction;
use App\Models\UserMeta;
use App\Models\UserLender;
use App\Models\VerifyToken;
use App\Models\UserBorrower;
use Modules\BasicKYC\Models\KycSessions;
use App\Enums\UserStatus;
use App\Enums\UserRoles;
use App\Mail\SystemEmail;
use App\Jobs\ProcessEmail;
use App\Filters\UserFilter;
use App\Services\AuthService;
use App\Services\Exports\UserCsvExport;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    private $authService;

    public function __construct(AuthService $authService)
    {
        $this->authService = $authService;
    }

    /**
     * @param UserFilter $filter
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @version 1.0.0
     * @since 1.0
     */
    public function index(UserFilter $filter, $userState = null)
    {
        $usersQuery = User::withTrashed()
            ->withoutSuperAdmin()
            ->orderBy('id', user_meta('user_order', 'desc'))
            ->filter($filter);

        if ($userState) {
            $usersQuery->where('status', $userState);
            $usersQuery->where('role', '<>', 'admin');
        } else {
            $usersQuery->where('role', '<>', 'admin')->whereNotIn('status', [UserStatus::INACTIVE]);
        }

        $users = $usersQuery->paginate(user_meta('user_perpage', 10))->onEachSide(0);

        return view('admin.user.list', compact('users'));
    }

    public function borrower(UserFilter $filter)
    {
        $usersQuery = UserBorrower::with('occupation')->withTrashed()->orderBy('id', 'desc');
        $users = $usersQuery->paginate(user_meta('user_perpage', 10))->onEachSide(0);
        $pageTitle = 'Borrower';

        return view('admin.user.list-2', compact('users', 'pageTitle'));
    }

    public function lender(UserFilter $filter)
    {
        $usersQuery = UserLender::with('lenderType')->withTrashed()->orderBy('id', 'desc');
        $users = $usersQuery->paginate(user_meta('user_perpage', 10))->onEachSide(0);
        $pageTitle = 'Lender';

        return view('admin.user.list-2', compact('users', 'pageTitle'));
    }

    public function loanRequest(UserFilter $filter)
    {
        $loansRequestQuery = LoanRequest::with(['user.kycSessions','offer','transactions'])->withTrashed()->orderBy('id', 'desc');
        $loans = $loansRequestQuery->paginate(user_meta('user_perpage', 10))->onEachSide(0);
        $pageTitle = 'Loan Request';

        return view('admin.user.list-loan-request', compact('loans', 'pageTitle'));
    }

    public function loanOffer(UserFilter $filter)
    {
        $offersQuery = LoanOffer::with(['user','loan'])->orderBy('id', 'desc');
        $offers = $offersQuery->paginate(user_meta('user_perpage', 10))->onEachSide(0);
        $pageTitle = 'Loan Offer';

        return view('admin.user.list-loan-offer', compact('offers', 'pageTitle'));
    }

    public function loanTransaction(UserFilter $filter)
    {
        $transactionsQuery = LoanTransaction::with(['loan.user'])->orderBy('id', 'desc');
        $transactions = $transactionsQuery->paginate(user_meta('user_perpage', 10))->onEachSide(0);
        $pageTitle = 'Loan Transactions';

        return view('admin.user.list-loan-transaction', compact('transactions', 'pageTitle'));
    }

    /**
     * @param UserFilter $filter
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @version 1.0.0
     * @since 1.0
     */
    public function administrator(UserFilter $filter)
    {
        $usersQuery = User::withTrashed()->orderBy('id', user_meta('user_order', 'desc'))->filter($filter);
        $usersQuery->whereIn('role', [UserRoles::ADMIN, UserRoles::SUPER_ADMIN]);
        $users = $usersQuery->paginate(user_meta('user_perpage', 10))->onEachSide(0);

        return view('admin.user.list', compact('users'));
    }

    public function showUserDetails($id, $type)
    {
        $user = User::with([
            'transactions',
            'activities',
            'miscTnx',
            'accounts',
            'referrals.referred'
        ])->find($id);

        if (blank($user)) {
            return redirect()->route('admin.users')->withErrors(['invalid' => __('Sorry, user id may invalid or not available.')]);
        }

        if (!blank($user) && $user->id == auth()->user()->id) {
            return redirect()->route('admin.profile.view');
        }

        if (!in_array($type, ['personal', 'transactions', 'misc', 'activities', 'referrals'])) {
            return redirect()->route('admin.users.details', [
                'id' => $user->id,
                'type' => 'personal'
            ])->withErrors([
                'invalid' => __('Sorry, your requested details is not available or invalid.')
            ]);
        }

        return view('admin.user.index', [
            'type' => $type,
            'user' => $user,
        ]);
    }

    public function showLoansDetails($type, $id)
    {
        if($type == 'request'){
            $data = LoanRequest::with(['user.kycSessions','offer','transactions'])->find($id);
        } else {
            $data = LoanOffer::with(['user','loan'])->find($id);
        }

        if (blank($data)) {
            if($type == 'request'){
                return redirect()->route('admin.loans.request')->withErrors(['invalid' => __('Sorry, Loan Request may invalid or not available.')]);
            } else{
                return redirect()->route('admin.loans.offer')->withErrors(['invalid' => __('Sorry, Loan Offer invalid or not available.')]);
            }
        }
        return view('admin.user.loan-index', [
            'type' => $type,
            'data' => $data,
        ]);
    }

    public function deleteOffer(Request $request, $loan_id = null)
    {
        $id = !empty($loan_id) ? $loan_id : $request->get('uid');
        $loan = LoanOffer::find($id);

        if (!blank($loan)) {

            $loan->delete();
            return response()->json([
                'msg' => __("The Loan Offer successfully deleted."), 'reload' => true,
            ]);

        } else {
            return response()->json([
                'type' => 'warning', 'msg' => __("An error occurred. Please try again.")
            ]);
        }
    }

    public function deleteTransaction(Request $request, $transaction_id = null)
    {
        $id = !empty($transaction_id) ? $transaction_id : $request->get('uid');
        $transaction = LoanTransaction::find($id);

        if (!blank($transaction)) {

            $transaction->delete();
            return response()->json([
                'msg' => __("The Loan Transaction successfully deleted."), 'reload' => true,
            ]);

        } else {
            return response()->json([
                'type' => 'warning', 'msg' => __("An error occurred. Please try again.")
            ]);
        }
    }

    public function makeLoanOffer(Request $request, $id)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'action.provider_name' => 'required',
                'action.interest_amount' => 'required|numeric',
                'action.interest_rate' => 'required|numeric',
                'action.repayment_amount' => 'required|numeric',
                'action.late_charges' => 'required|numeric'
            ]
        );

        if ($validator->fails()) {
            throw ValidationException::withMessages(["error" => $validator->errors()->first()]);
        }

        $findLoan = loanRequest::with(['offer','transactions'])->find($id);

        if (!$findLoan) {
            throw ValidationException::withMessages(["warning" => __("Loan Not Found.")]);
        }

        try {
            $LoanOffer = new LoanOffer();
            $LoanOffer->user_id = auth()->user()->id;
            $LoanOffer->loan_id = $id;
            $LoanOffer->provider_name = $request->action['provider_name'];
            $LoanOffer->interest_amount =  ($findLoan->loan_amount * ($request->action['interest_rate']/100)) * $findLoan->loan_duration ;
            $LoanOffer->interest_rate = $request->action['interest_rate'];
            $LoanOffer->repayment_amount = $findLoan->loan_amount + $LoanOffer->interest_amount;
            $LoanOffer->late_charges = $request->action['late_charges'];
            $LoanOffer->status = 'STARTED';
            $LoanOffer->save();
            $findLoan->status = 'Offer Received';
            $findLoan->save();
            return response()->json(["type" => "success", "reload" => true, "title" => __("Make Loan Offer"), "msg" => __("Make Loan Offer has been successfully created.")]);
        } catch (\Exception $e) {
            save_error_log($e);
            throw ValidationException::withMessages(["error" => __("Sorry, we are unable to proceed your request.")]);
        }


    }

    public function makeLoanTransaction(Request $request, $id)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'action.transaction_hash' => 'required'
            ]
        );

        if ($validator->fails()) {
            throw ValidationException::withMessages(["error" => $validator->errors()->first()]);
        }

        $findLoan = loanRequest::with(['offer','transactions'])->find($id);

        if (!$findLoan) {
            throw ValidationException::withMessages(["warning" => __("Loan Not Found.")]);
        }

        try {
            $loanTransaction = new loanTransaction();
            $loanTransaction->loan_id = $id;
            $loanTransaction->transaction_hash = $request->transaction_hash;
            $loanTransaction->status = 'Transaction Received';
            $loanTransaction->save();

            $findLoan->status = 'Pending Repayment';
            $findLoan->save();
            return response()->json(["type" => "success", "reload" => true, "title" => __("Make Loan Transaction Hash"), "msg" => __("Make Loan Transaction has been successfully created.")]);
        } catch (\Exception $e) {
            save_error_log($e);
            throw ValidationException::withMessages(["error" => __("Sorry, we are unable to proceed your request.")]);
        }


    }

    public function updateLoanStatus(Request $request, $id)
    {
        $action = $request->action;
        if (empty($action) || !in_array($action, ["active", "repaid", "deactive","pending-test-amount"])) {
            throw ValidationException::withMessages(["error" => __("Something went wrong. Please reload the page and try again.")]);
        }

        $findLoan = loanRequest::with(['offer','transactions'])->find($id);

        if (!$findLoan) {
            throw ValidationException::withMessages(["warning" => __("Loan Not Found.")]);
        }
        $user = User::find($findLoan->user_id);
        if (!$user){
            $user = null;
        }


        try {

            if ($action == 'active'){
                if (!$findLoan->offer){
                    $created_at = null;
                } else {
                    $userLender = User::find($findLoan->offer->user_id);
                    if (!$userLender){
                        $userLender = null;
                    }
                    $kycLender = KycSessions::where('user_id', $userLender->id)->first();
                    if (!$kycLender){
                        $kycLender = null;
                    }
                    if ($userLender->role == 'admin'){
                        $lender_address = '';
                    } else {
                        $lender_address = $kycLender->profile['address_line_1'].' '.$kycLender->profile['city'].' '.$kycLender->profile['zip'];
                    }

                    $userBorrower = User::find($findLoan->user_id);
                    if (!$userBorrower){
                        $userBorrower = null;
                    }
                    $kycBorrower = KycSessions::where('user_id', $userBorrower->id)->first();
                    if (!$kycBorrower){
                        $kycBorrower = null;
                        $borrower_name = null;
                        $borrower_address = null;
                    } else {
                        $borrower_name = $kycBorrower->profile['first_name'].' '.$kycBorrower->profile['last_name'];
                        $borrower_address = $kycBorrower->profile['address_line_1'].' '.$kycBorrower->profile['city'].' '.$kycBorrower->profile['zip'];

                    }
                    $loan_amount = $findLoan->loan_amount;
                    $interest_amount = $findLoan->offer->interest_amount;
                    $loan_duration = $findLoan->loan_duration;
                    $created_at = Carbon::parse($findLoan->offer->created_at)->format(sys_settings('date_format'));
                }
                $data = [
                    'type' => 'Loan',
                    'loan_id' => $findLoan->id,
                    'created_at' => $created_at,
                    'provider_name' => $findLoan->offer->provider_name ?? null,
                    'lender_address' => $lender_address ?? null,
                    'borrower_name' => $borrower_name ?? null,
                    'borrower_address' => $borrower_address ?? null,
                    'loan_amount' => $loan_amount ?? null,
                    'interest_amount' => $interest_amount ?? null,
                    'loan_duration' => $loan_duration ?? null,
                ];
                ProcessEmail::dispatch('user-loan-active-admin', $user, null, $data, null);
                $findLoan->status = 'Active';
            } elseif ($action == 'pending-test-amount') {
                $findLoan->status = 'Pending Test Amount';
            } elseif ($action == 'deactive') {
                $findLoan->status = 'Deactive';
            } else {
                $findLoan->status = 'Repaid';
            }

            $findLoan->save();

            return response()->json(["type" => "success", "reload" => true, "title" => __("Loan Request"), "msg" => __("Loan Request Status ".$findLoan->status." has been successfully changed.")]);
        } catch (\Exception $e) {
            save_error_log($e);
            throw ValidationException::withMessages(["error" => __("Sorry, we are unable to proceed your request.")]);
        }


    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws ValidationException
     * @version 1.0.0
     * @since 1.0
     */
    public function saveUser(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|min:3|max:190',
            'email' => 'required|email|max:190|unique:users',
            'password' => 'nullable|min:6',
        ]);

        DB::beginTransaction();

        try {
            $data = $request->all();
            $data['name'] = strip_tags($data['name']);
            $data['registration_method'] = 'direct';
            $data['password'] = $request->get('password') ?? Str::random(8);
            $verified = $request->get('verified') != 'on';
            $isAdmin = ($request->get('role')==UserRoles::ADMIN);

            $redirect = ($isAdmin) ? route('admin.users.administrator') : route('admin.users');

            $user = $this->authService->createUser($data, $verified);

            if (!$user) {
                throw ValidationException::withMessages(['invalid' => __('Unable to create new account, please try again.')]);
            }

            if ($verified) {
                UserMeta::insert([
                    ['user_id' => $user->id, 'meta_key' => 'email_verified', 'meta_value' => Carbon::now()],
                    ['user_id' => $user->id, 'meta_key' => 'email_verified_last', 'meta_value' => Carbon::now()],
                ]);
            }

            ProcessEmail::dispatch('user-registration-admin', $user, null, null, $data);

            DB::commit();

            return response()->json(['url' => $redirect, 'msg' => __("New user account added successfully.")]);
        } catch (\Exception $e) {
            DB::rollBack();
            save_mailer_log($e, 'user-registration-admin');
            throw ValidationException::withMessages(['invalid' => __('Unable to create user, please try again.')]);
        }
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws ValidationException
     * @version 1.0.0
     * @since 1.0
     */
    public function updateAction(Request $request)
    {
        $actionType = $request->get('action');

        switch ($actionType) {
            case 'suspend':
                return $this->suspendUser($request);
                break;
            case 'active':
                return $this->activeUser($request);
                break;
            case 'password':
                return $this->resetPassword($request);
                break;
            case 'locked':
                return $this->statusUpdate($actionType, $request);
                break;
            case 'verification':
                return $this->resendVerificationEmail($request);
                break;
        }

        throw ValidationException::withMessages(['invalid' => __('An error occurred. Please try again.'.$actionType)]);
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws ValidationException
     * @version 1.0.0
     * @since 1.0
     */
    public function resetPassword(Request $request)
    {
        $userID = ($request->get('user_id')) ? (int) $request->get('user_id') : (int) $request->get('uid');
        $isReload = ($request->get('reload') == true || $request->get('reload') == 'true') ? true : false;

        try {
            $user = User::find($userID);
            $password = Str::random(8);

            if ($user) {
                if ($user->role == UserRoles::SUPER_ADMIN) {
                    return response()->json(['type' => 'error', 'msg' => __('Sorry, you do not have enough permissions to reset password the super admin account.')], 202);
                }

                $user->password = Hash::make($password);
                $user->save();

                ProcessEmail::dispatch('users-admin-reset-password', $user, null, null, ['random_pass' => $password]);

                return response()->json(['title' => 'Password Reset', 'msg' => __('The password has been reset successfully.'), 'reload' => $isReload]);
            }
        } catch (\Exception $e) {
            // IO: NEED TO ROLLBACK
            save_mailer_log($e, 'users-admin-reset-password');
            throw ValidationException::withMessages(['invalid' => __('An error occurred. Please try again.')]);
        }
        throw ValidationException::withMessages(['invalid' => __('User not found or invalid user account id.')]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws ValidationException
     * @version 1.0.0
     * @since 1.0
     */
    public function sendEmail(Request $request)
    {
        $userID = ($request->get('send_to')) ? (int) $request->get('send_to') : (int) $request->get('uid');

        $user = User::find($userID);
        if (!blank($user)) {
            $data = [
                "subject" => sanitize_input($request->get('subject')),
                "greeting" => sanitize_input($request->get('greeting')),
                "message" => $request->get('message')
            ];

            if (isset($data['greeting']) && empty($data['greeting'])) {
                $data['greeting'] = __("Hello");
            }

            if (isset($data['subject']) && empty($data['subject'])) {
                $data['subject'] = __("New message from :site", ['site' => site_info('name')]);
            }

            try {
                Mail::to($user->email)->send(new SystemEmail($data, 'users.custom-email'));
            } catch (\Exception $e) {
                save_mailer_log($e, 'send-email-user');
                throw ValidationException::withMessages(['invalid' => __('Sorry, we are unable to send email to user.')]);
            }

            return response()->json(['title' => 'Message Sent', 'msg' => __('Your message has been sent successfully.')]);
        }

        throw ValidationException::withMessages(['invalid' => __('An error occurred. Please try again.')]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @version 1.0.0
     * @since 1.0
     */
    public function suspendUser(Request $request)
    {
        $userID = ($request->get('user_id')) ? (int) $request->get('user_id') : (int) $request->get('uid');
        $isReload = ($request->get('reload')) ? $request->get('reload') : false;

        $user = User::find($userID);
        if (!blank($user)) {
            if ($user->status == UserStatus::INACTIVE) {
                throw ValidationException::withMessages(['invalid' => __('User account may not verified or inactive.')]);
            }

            if ($user->role == UserRoles::SUPER_ADMIN) {
                return response()->json(['type' => 'error', 'msg' => __('Sorry, you do not have enough permissions to suspend the super admin account.')], 202);
            }

            $user->status = UserStatus::SUSPEND;
            $user->save();
            return response()->json([ 'title' => 'Account Suspended', 'msg' => __('User has been successfully suspended.'), 'state' => __('Suspend'), 'reload' => $isReload ]);
        }

        throw ValidationException::withMessages(['invalid' => __('User not found or invalid user account id.')]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @version 1.0.0
     * @since 1.0
     */
    public function activeUser(Request $request)
    {
        $userID = ($request->get('user_id')) ? (int) $request->get('user_id') : (int) $request->get('uid');
        $isReload = ($request->get('reload')) ? $request->get('reload') : false;

        $user = User::find($userID);
        if (!blank($user)) {
            if ($user->status == UserStatus::INACTIVE) {
                throw ValidationException::withMessages(['invalid' => __('User account may not verified or inactive.')]);
            }
            $user->status = UserStatus::ACTIVE;
            $user->save();
            return response()->json([ 'title' => 'Account Actived', 'msg' => __('User has been successfully actived.'), 'state' => __('Active'), 'reload' => $isReload ]);
        }

        throw ValidationException::withMessages(['invalid' => __('User not found or invalid user account id.')]);
    }

    /**
     * @param $status
     * @param $userID
     * @return \Illuminate\Http\JsonResponse
     * @version 1.0.0
     * @since 1.0
     */
    public function statusUpdate($status, Request $request)
    {
        $uStatus = false;
        $userID = ($request->get('user_id')) ? (int) $request->get('user_id') : (int) $request->get('uid');
        $isReload = ($request->get('reload')) ? $request->get('reload') : false;

        switch ($status) {
            case 'active':
                $uStatus = UserStatus::ACTIVE;
            break;

            case 'suspend':
                $uStatus = UserStatus::SUSPEND;
            break;

            case 'locked':
                $uStatus = UserStatus::LOCKED;
            break;

            case 'deleted':
                $uStatus = UserStatus::DELETED;
            break;
        }

        $user = User::find($userID);
        if (!blank($user) && $uStatus) {
            if ($user->status == UserStatus::INACTIVE) {
                throw ValidationException::withMessages(['invalid' => __('User account may not verified or inactive.')]);
            }

            if ($user->role == UserRoles::SUPER_ADMIN) {
                return response()->json(['type' => 'error', 'msg' => __('Sorry, you do not have enough permissions to :what the super admin account.', ['what' => strtolower(__(ucfirst($status))) ]) ], 202);
            }

            if ($status=='deleted') {
                $user->deleted_at = Carbon::now();
            }
            $user->status = $uStatus;
            $user->save();
            return response()->json([ 'title' => "Status Updated", 'msg' => __("User status has been set ':what'.", ['what' => strtolower(__(ucfirst($status)))]), 'reload' => $isReload ]);
        }

        throw ValidationException::withMessages(['invalid' => __('User not found or invalid user account id.')]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws ValidationException
     * @version 1.0.0
     * @since 1.0
     */
    public function bulkAction(Request $request)
    {
        $this->validate($request, [
            'action' => 'required|string',
            'users' => 'required|array|min:1',
        ]);

        $action = $request->get('action');
        $users = $request->get('users');

        if ($action && $users) {
            if ($action ==='removed') {
                $userQuery = User::whereIn('id', $users)
                    ->where('status', [UserStatus::INACTIVE])
                    ->whereNotIn('role', [UserRoles::SUPER_ADMIN])
                    ->where('id', '<>', auth()->user()->id);
            } else {
                $userQuery = User::whereIn('id', $users)
                    ->whereNotIn('status', [UserStatus::INACTIVE])
                    ->whereNotIn('role', [UserRoles::SUPER_ADMIN])
                    ->where('id', '<>', auth()->user()->id);
            }

            if (!blank($userQuery)) {
                switch ($action) {
                    case 'suspended':
                        $userQuery->update([ 'status' => UserStatus::SUSPEND ]);
                        break;
                    case 'locked':
                        $userQuery->update([ 'status' => UserStatus::LOCKED ]);
                        break;
                    case 'actived':
                        $userQuery->update([ 'status' => UserStatus::ACTIVE ]);
                        break;
                    case 'deleted':
                        $userQuery->update([ 'status' => UserStatus::DELETED, 'deleted_at' => Carbon::now() ]);
                        break;
                    case 'removed':
                        $ids = $userQuery->pluck('id')->toArray();
                        $userQuery->forceDelete();
                        UserMeta::whereIn('user_id', $ids)->delete();
                        break;
                }
                return response()->json([ 'title' => 'Bulk Updated', 'msg' => __('All the selected users has been :what.', ['what' => __($action)]), 'reload' => true ]);
            }
            return response()->json([ 'type' => 'info', 'msg' => __('Failed to update the selected users.') ]);
        }
        throw ValidationException::withMessages(['invalid' => __('An error occurred. Please try again.')]);
    }

    public function resendVerificationEmail(Request $request)
    {
        $userId = $request->get('uid');
        $user = User::WithoutSuperAdmin()->find($userId);

        if (blank($user) || $user->status != UserStatus::INACTIVE) {
            throw ValidationException::withMessages(['invalid' => __('Invalid User!')]);
        }

        try {
            $this->authService->generateNewToken($user, true);
            $user = $user->fresh();
            ProcessEmail::dispatch('users-confirm-email', $user);
        } catch (\Exception $e) {
            save_mailer_log($e, 'users-confirm-email');
            return response()->json(['type' => 'error', 'msg' => __('Sorry new verification email sending failed!') ], 202);
        }

        return response()->json([ 'title' => 'Verification Email Sent', 'msg' => __('A new verification email has been sent to user!')]);
    }

    /**
     * @param Request $request
     * @throws ValidationException
     * @version 1.0.0
     * @since 1.1.2
     */
    public function exportUsers(Request $request)
    {
        if (is_demo_user()) {
            return back()->with(['warning' => 'Sorry, you do not have enough permissions.' ]);
        }

        $request->validate([
            'type' => 'required|in:entire,minimum,compact'
        ]);

        $export = new UserCsvExport;
        $export->download($request->type);
    }

    /**
     * @param Request $request
     * @throws ValidationException
     * @version 1.0.0
     * @since 1.1.2
     */
    public function getReferralTree(Request $request)
    {
        $userId = $request->get('id');
        $user = User::with(['referrals.referred'])->find($userId);
        if (!blank($user)) {
            return view('admin.user.referral-tree.tree', ['user' => $user])->render();
        }
    }
}
