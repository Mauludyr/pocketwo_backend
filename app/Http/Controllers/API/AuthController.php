<?php

namespace App\Http\Controllers\API;

use Exception;
use Carbon\Carbon;
use App\Models\User;
use App\Models\UserLender;
use App\Models\VerifyToken;
use App\Models\UserBorrower;
use App\Enums\UserRoles;
use App\Enums\UserStatus;
use App\Services\AuthService;
use App\Jobs\ProcessEmail;
use App\Helpers\ResponseHelper;
use App\Http\Requests\RegistrationRequest;
use App\Http\Requests\ResetPasswordRequest;
use App\Http\Controllers\Controller;
use App\Http\Resources\API\UserResource;
use App\Services\SettingsService;
use Illuminate\Auth\Events\PasswordReset;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Requests\LoginRequest;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

class AuthController extends Controller
{
    private $auth;

    public function __construct(AuthService $authService, SettingsService $settingsService)
    {
        $this->auth = $authService;
        $this->settingsService = $settingsService;
        $this->responseHelper = new ResponseHelper;
    }

    protected function respondWithToken($token, $user) 
    {
        return response()->json([
            'message' => 'Success',
            'access_token' => $token,
            'token_type' => 'bearer',
            'user' => new UserResource($user)
        ]);
    }

    private function getVerifyToken($request) 
    {
        $verify = $request->get('token'); 
        $email  = $this->getVerifyEmail($request);

        if ($verify) {
            $token  = Str::replaceLast($email, '', $verify);
            $verifyToken = VerifyToken::where('token', $token)->first();

            return (!empty($verifyToken)) ? $verifyToken : false;
        }

        return false;
    }

    private function getVerifyEmail($request) 
    {
        $verify = $request->get('token'); 
        return ($verify) ? substr($verify, -32) : '';
    }

    private function countInvalidAttempt(Request $request, User $user)
    {
        // if ($request->session()->missing('invalidAttempts')) {
        //     session(['invalidAttempts' => 1]);
        // } else {
        //     $request->session()->increment('invalidAttempts', $incrementBy = 1);
        // }

        // if (session('invalidAttempts') >= 5) {
        //     $request->session()->forget('invalidAttempts');
        //     ProcessEmail::dispatch('users-unusual-login', $user);
        // }
    }

    public function register(RegistrationRequest $request)
    {
        $emailMetaCount = $this->settingsService->emailMetaCount($request->email);
        if ($emailMetaCount > 0) {
            return response()->json($this->responseHelper->errorCustom(422, 'The chosen email is already registered with us. Please use a different email address.'), 422);
        }

        DB::beginTransaction();
        try {
            $data = array_map('strip_tags_map', $request->only('name', 'confirmation')) + $request->all();
            $user = $this->auth->createUser($data);

            $check = User::where('email', $request->email)
                ->first();

            if (!$user) {
                return response()->json($this->responseHelper->errorCustom(422, 'An error occurred during registration, please try again later. If the issues continues, contact us.'), 422);
            }

            if (User::count() > 1 && mandatory_verify()) {
                ProcessEmail::dispatch('users-confirm-email', $user);
            }

            DB::commit();
            return response()->json($this->responseHelper->successWithData($user), 200);
        } catch (\Exception $e) {
            DB::rollBack();

            return response()->json($this->responseHelper->errorCustom(422, $e->getMessage()), 422);
        }
    }
    
    public function login(LoginRequest $request)
    {

        $user = User::where('email', $request->input('email'))->first();
        $remember = $request->get('remember') == 'on';
        $password = $request->input('password');
        
        if (!$user) {
            return response()->json($this->responseHelper->errorCustom(404, 'The email address you entered is incorrect or the account does not exist.'), 404);
        }

        if ($user->status == UserStatus::SUSPEND) {
            return response()->json($this->responseHelper->errorCustom(404, 'We are sorry, this account has been temporarily suspended. Please contact us for assistance.'), 404);
        }
        
        if (mandatory_verify() && $user->role != UserRoles::SUPER_ADMIN) {
            if ($user->is_verified) {
                if ($user->status != UserStatus::ACTIVE) {
                    return response()->json($this->responseHelper->errorCustom(404, 'We are sorry, this account may locked out or not active. Please contact us for assistance.'), 404);
                }
            } else {
                    return response()->json($this->responseHelper->errorCustom(404, 'Your email address has not been verified yet! In order to start using your account, you need to verify your email address first.'), 404);
            }
        }

        if (!$this->auth->checkPassword($user, $password)) {
            // $this->countInvalidAttempt($request, $user);
            return response()->json($this->responseHelper->errorCustom(404, 'The password you entered is incorrect or the account does not exist.'), 404);
        }

        $this->auth->loginUser($user, $remember);

        $welcome = false;

        if(empty($user->meta('first_login_at'))) {
            $user->user_metas()->create(['meta_key' => 'first_login_at', 'meta_value' => now()]);
            $welcome = true;
        }
        if ($user->role == UserRoles::SUPER_ADMIN){
            $token = $user->createToken('super-admin', ['super-admin-access'])->plainTextToken;
        }elseif ($user->role == UserRoles::ADMIN){
            $token = $user->createToken('admin', ['admin-access'])->plainTextToken;
        }else{
            $token = $user->createToken('user', ['user-access'])->plainTextToken;
        }

        return $this->respondWithToken($token, $user);
    }

    public function countries(Request $request)
    {
        try{
            $countries = filtered_countries();
            return response()->json($this->responseHelper->successWithData($countries), 200);
        } catch (\Exception $e) {
            return response()->json($this->responseHelper->errorCustom(422, $e->getMessage()), 422);
        }
    }

    public function lender(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'type' => ['required'],
            'email' => ['required', 'string', 'email', 'unique:user_lenders,email']
        ]);

        if ($validator->fails()) {
            return $this->responseHelper->errorCustom(422, $validator->errors()->first());
        }

        try {
            $check = UserLender::where('email', $request->email)
                ->first();

            if ($check) {
                return $this->responseHelper->errorCustom(422, 'The email has already exists.');
            }

            $user = new UserLender();
            $user->type_id = $request->type;
            $user->email = $request->email;
            $user->save();

            return response()->json($this->responseHelper->successWithoutData('Thank you! We’ll let you know when we’ve launched our app'), 200);
        } catch (\Exception $e) {
            return response()->json($this->responseHelper->errorCustom(422, $e->getMessage()), 422);
        }
    }

    public function borrower(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'occupation' => ['required'],
            'age' => ['required', 'string'],
            'email' => ['required', 'string', 'email', 'unique:user_borrowers,email'],
        ]);

        if ($validator->fails()) {
            return $this->responseHelper->errorCustom(422, $validator->errors()->first());
        }
        
        try {
            $check = UserBorrower::where('email', $request->email)
                ->first();

            if ($check) {
                return response()->json($this->responseHelper->errorCustom(422, 'The email has already exists.'), 422);
            }

            $user = new UserBorrower();
            $user->occupation_id = $request->occupation;
            $user->age = $request->age;
            $user->email = $request->email;
            $user->save();
            
            return response()->json($this->responseHelper->successWithoutData('Thank you! We’ll let you know when we’ve launched our app'), 200);
        } catch (\Exception $e) {
            return response()->json($this->responseHelper->errorCustom(422, $e->getMessage()), 422);
        }
    }

    public function forgetPassword(Request $request)
    {

        try {
            $this->validate($request, [
                'email' => ['required', 'email', 'exists:users,email']
            ]);

            $user = User::where('email', $request->input('email'))->first();

            if ($user) {
                $verifyToken = VerifyToken::where('email', $user->email)->first();
                $verifyToken->verify = null;
                $verifyToken->token = random_hash($user->email);
                $verifyToken->save();

                ProcessEmail::dispatch('users-reset-password', $user);
            }
        } catch (\Exception $e) {
            return response()->json($this->responseHelper->errorCustom(422, "We couldn't find the account that associate with the email address you entered"), 422);
        }

        return response()->json($this->responseHelper->successWithoutData('Reset Password has been sent'), 200);
    }

    public function resetPassword(ResetPasswordRequest $request)
    {
        DB::beginTransaction();
        try {
            $user = User::where('email', $request->get('email'))->first();

            $verifyToken = $this->getVerifyToken($request);
            if ($verifyToken) {
                if ($verifyToken->token !== $user->verify_token->token) {
                    return response()->json($this->responseHelper->errorCustom(422, "Something wrong"), 422);
                }
    
                $user->password = Hash::make($request->input('password'));
                $user->save();
                $user->user_metas()->updateOrCreate([
                    'meta_key' => 'last_password_changed',
                    'meta_value' => now()->timestamp
                ]);
    
                $verifyToken = VerifyToken::where('email', $user->email)->first();
                $verifyToken->token = null;
                $verifyToken->verify = Carbon::now();
                $verifyToken->save();
    
                DB::commit();
            }
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json($this->responseHelper->errorCustom(422, "Sorry, the account does not exist or invalid"), 422);
        }

        try {
            ProcessEmail::dispatch('users-change-password-success', $user);
        } catch (\Exception $e) {
            save_mailer_log($e, 'users-change-password-success');
        }
        return response()->json($this->responseHelper->successWithoutData('Reset Password has been success'), 200);
    }

    public function resendVerify(Request $request)
    {
        $this->validate($request, [
            'email' => ['required', 'email']
        ]);
        
        $user = User::where('email', $request->input('email'))->first();

        if(empty($user)){
            return response()->json($this->responseHelper->errorCustom(422, "We couldn't find the account that associate with the email address you entered"), 422);
        }

        try {
            $this->auth->generateNewToken($user);
            ProcessEmail::dispatch('users-confirm-email', $user);
            return response()->json($this->responseHelper->successWithoutData('We have emailed you a confirmation link to your email. Please check your inbox and confirm.'), 200);
        }catch (\Exception $e){
            save_mailer_log($e, 'users-confirm-email');
            return response()->json($this->responseHelper->errorCustom(422, "We are unable to send the verification link to your email."), 422);
        }
    }
}
