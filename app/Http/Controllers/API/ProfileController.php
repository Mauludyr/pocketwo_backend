<?php

namespace App\Http\Controllers\API;

use Exception;
use Modules\BasicKYC\Models\KycSessions;
use Carbon\Carbon;
use App\Models\User;
use App\Models\UserMeta;
use App\Models\LoanRequest;
use App\Helpers\ResponseHelper;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

class ProfileController extends Controller
{

    public function __construct()
    {
        $this->responseHelper = new ResponseHelper;
    }

    public function getProfile()
    {
        $user = KycSessions::where('user_id', auth()->user()->id)->first();
        return response()->json($this->responseHelper->successWithData($user), 200);
    }

    public function getLoan()
    {
        $user = auth()->user()->loan_limit;
        return response()->json($this->responseHelper->successWithData($user), 200);
    }

    public function getGuide()
    {
        $data = [
            [
                "id" => 1, 
                "title" => "How do I request a loan?",
                "content" => "In this article, we will be looking at how you can request for a loan after creating and verifying your account.",
                "image_url" => "https://pocketwo.com/images/blog/images/1.png",
                "link_url" => "https://pocketwo.com/blog/request-a-loan"


            ],
            [
                "id" => 2, 
                "title" => "How can I receive my loan?",
                "content" => "In this article, we will be looking at how you can receive your loan after creating and verifying your account, and receiving a loan offer.",
                "image_url" => "https://pocketwo.com/images/blog/images/2/1.png",
                "link_url" => "https://pocketwo.com/blog/receive-loan"


            ]
        ];
        return response()->json($this->responseHelper->successWithData($data), 200);
    }

    public function updateProfile(Request $request)
    {
        $rules = [];
        $messages = [];
        // rules
        $rules["first_name"]            = "required|string";
        $rules["last_name"]             = "required|string";
        $rules["gender"]                = "required|string";
        $rules["dob"]                   = "required|date_format:m/d/Y";
        $rules["phone"]                 = "required|string";
        $rules["nationality"]           = "required|string";
        $rules["country_of_residence"]  = "required|string";
        $rules["year_in_country"]       = "required|string";
        $rules["address_line_1"]        = "required|string";
        $rules["address_line_2"]        = "nullable";
        $rules["city"]                  = "required|string";
        $rules["zip"]                   = "required|string";
        $rules["twitter"]               = "nullable|string";
        $rules["facebook"]              = "nullable|string";
        $rules["instagram"]             = "nullable|string";
        $rules["tiktok"]                = "nullable|string";
        $rules["education"]             = "required|string";
        $rules["marital_status"]        = "required|string";
        $rules["children"]              = "required|string";
        $rules["monthly_income"]        = "required|string";
        $rules["source_income"]         = "required|string";
        $rules["monthly_liabilities"]   = "nullable|string";

        // $messages
        $messages["first_name.required"]            = __("Please enter your full First Name.");
        $messages["last_name.required"]             = __("Please enter your full Last Name.");
        $messages["gender.required"]                = __("Please Choose your Gender.");
        $messages["dob.required"]                   = __("Please Input your Date of Birth.");
        $messages["dob.date_format"]                = __("Enter Date of Birth in this 'mm/dd/yyyy' format.");
        $messages["phone.required"]                 = __("Please Input your Phone Number.");
        $messages["nationality.required"]           = __("Please Input your nationality Number.");
        $messages["country_of_residence.required"]  = __("Please Input your Country of Residence Number.");
        $messages["year_in_country.required"]       = __("Please Input your Year in Country.");
        $messages["address_line_1.required"]        = __("Please Input your Address Line 1.");
        $messages["city.required"]                  = __("Please Choose your City.");
        $messages["zip.required"]                   = __("Please Input your Zip Code.");
        $messages["education.required"]             = __("Please Choose your Education.");
        $messages["marital_status.required"]        = __("Please Choose your Marital Status.");
        $messages["children.required"]              = __("Please Choose your Marital Status.");
        $messages["monthly_income.required"]        = __("Please Input your Monthly Income.");
        $messages["source_income.required"]         = __("Please Choose your Source Income.");
        // $messages["monthly_liabilities.required"]   = __("Please Choose your Monthly Liability.");

        $validatedData = request()->validate($rules, $messages);
        $updateData = Arr::only($validatedData, ["first_name","last_name","gender","dob","phone","nationality","country_of_residence","year_in_country","address_line_1","address_line_2","city","zip","twitter","facebook","instagram","tiktok","education","marital_status","children","monthly_income","source_income","monthly_liabilities",]);
        $updateData = array_map("strip_tags_map", $updateData);

        $getSession = KycSessions::where("user_id", auth()->user()->id)->where('status','approve')->first();
        if (!$getSession) {
            return response()->json($this->responseHelper->errorCustom(422, "Data Not Found"), 422);
        }

        try {
            
            $getSession->profile = array_merge(data_get($getSession, "profile", []), $updateData);
            $getSession->save();
            return response()->json($this->responseHelper->successWithData($getSession), 200);
        } catch (\Exception $e) {
            save_error_log($e);
            return response()->json($this->responseHelper->errorCustom(422, $e->getMessage()), 422);
        }
    }
}
