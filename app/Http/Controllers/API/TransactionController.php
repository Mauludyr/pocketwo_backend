<?php

namespace App\Http\Controllers\API;

use App\Enums\TransactionStatus;
use App\Enums\TransactionType;
use App\Enums\PaymentMethodStatus;
use App\Enums\TransactionCalcType;
use App\Enums\WithdrawMethodStatus;

use App\Helpers\MsgState;
use App\Jobs\ProcessEmail;
use App\Helpers\ResponseHelper;
use App\Filters\TransactionFilter;

use App\Models\Setting;
use App\Models\Account;
use App\Models\PaymentMethod;
use App\Models\Transaction;
use App\Models\UserAccount;
use App\Models\WithdrawMethod;
use App\Models\Occupation;
use App\Models\LenderType;

use App\Services\Transaction\TransactionProcessor;
use App\Services\Transaction\TransactionService;
use App\Services\Withdraw\WithdrawProcessor;

use Brick\Math\BigDecimal;
use Illuminate\Support\Arr;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use App\Http\Controllers\Controller;

class TransactionController extends Controller
{
    public function __construct()
    {
        $this->responseHelper = new ResponseHelper;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function Dashboard()
    {
        if (!auth()->user()->tokenCan('user-access')) {
            return response()->json([
                'message' => 'Unauthorized'
            ], 403);
        }

        $transactionService = new TransactionService();

        $interestPayload = [
            'base_currency' => 'USDT',
            'currency' => 'USDT',
            'exchange_rate' => 1,
            'pay_to' => 'TRaUbhsvSy8pUimxZF3pW7eZoFYd6HhtL1'
        ];

        $getInterestData = $transactionService->createInterestTransaction($interestPayload);

        $currentInterestRate = $getInterestData['currentInterestRate'];
        $interestTimeFrame = $getInterestData['interestTimeFrame'];
        $balanceTimeFrame = $getInterestData['balanceTimeFrame'];
        $dateTimeFrame = $getInterestData['dateTimeFrame'];
        $currentBalance = $getInterestData['currentBalance'];
        $currentInterest = $getInterestData['currentInterest'];

        $paymentMethods = PaymentMethod::where('status', PaymentMethodStatus::ACTIVE)
            ->get()->keyBy('slug')->toArray();

        $interestRate = Setting::where('key','like','%'.'interest_rate'.'%')->orderBy('id','desc')->pluck('value')->first();

        $recentTransactions = Transaction::with(['ledger'])
            ->whereIn('status', [TransactionStatus::ONHOLD, TransactionStatus::CONFIRMED, TransactionStatus::COMPLETED])
            ->whereNotIn('type', [TransactionType::REFERRAL])
            ->where('user_id', auth()->user()->id)
            ->orderBy('id', 'desc')
            ->limit(5)->get();


        $data = [
            'recentTransactions' => $recentTransactions,
            'currentInterestRate' => $currentInterestRate,
            'interestTimeFrame' => $interestTimeFrame,
            'balanceTimeFrame' => $balanceTimeFrame,
            'dateTimeFrame' => $dateTimeFrame,
            'currentBalance' => $currentBalance,
            'currentInterest' => $currentInterest,
            'interestRate' => $interestRate,
        ];
        return response()->json($this->responseHelper->successWithData($data), 200);
    }

    /**
     * @param Request $request
     * @param TransactionFilter $filter
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \ReflectionException
     * @version 1.0.0
     * @since 1.0
     */
    public function TransactionList(Request $request, TransactionFilter $filter)
    {
        $tnxTypes = get_enums(TransactionType::class, false);
        $tnxStates = Arr::except(get_enums(TransactionStatus::class, false), ['NONE', 'ONHOLD', 'CONFIRMED', 'PENDING']);

        $scheduledCount = Transaction::loggedUser()
            ->whereIn('status', [TransactionStatus::PENDING, TransactionStatus::CONFIRMED, TransactionStatus::ONHOLD])
            ->whereNotIn('type', [TransactionType::REFERRAL])
            ->count();
        $scheduled = ($request->has('view') && $request->get('view') == 'scheduled') ? true : false;

        $orderBy = $scheduled ? 'id' : 'completed_at';
        $sortBy = $scheduled ? 'asc' : 'desc';

        $query = Transaction::loggedUser()->orderBy($orderBy, $sortBy)->orderBy('created_at', 'desc')->whereNotIn('status', [TransactionStatus::NONE]);

        if (!$scheduled && blank($request->get('query')) && blank($request->get('filter'))) {
            $query->where('status', TransactionStatus::COMPLETED);
        }

        if ($scheduled || ($request->get('filter') == true && $request->get('type') != TransactionType::REFERRAL)) {
            $query->whereNotIn('type', [TransactionType::REFERRAL]);
        }

        $transactions = $query->filter($filter)
            ->paginate(user_meta('tnx_perpage', 10))
            ->onEachSide(0);

        $data = [
            'transactions' => $transactions,
            'tnxTypes' => $tnxTypes,
            'tnxStates' => $tnxStates,
            'scheduledCount' => $scheduledCount,
        ];
        return response()->json($this->responseHelper->successWithData($data), 200);
    }

    public function occupation()
    {
        $data = Occupation::get();
        return response()->json($this->responseHelper->successWithData($data), 200);
    }

    public function lenderType()
    {
        $data = lenderType::get();
        return response()->json($this->responseHelper->successWithData($data), 200);
    }
}
