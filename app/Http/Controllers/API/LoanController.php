<?php

namespace App\Http\Controllers\API;

use Exception, Log;
use Carbon\Carbon;
use App\Models\User;
use Modules\BasicKYC\Models\KycSessions;
use App\Models\LoanRequest;
use App\Models\LoanOffer;
use App\Models\loanTransaction;
use App\Helpers\ResponseHelper;
use App\Http\Controllers\Controller;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

class LoanController extends Controller
{

    public function __construct()
    {
        $this->pathLoan = "loan-document";
        $this->responseHelper = new ResponseHelper;
    }

    private function addNewLoan()
    {
        $userId = auth()->user()->id;
        $loanRequest = new LoanRequest();
        $loanRequest->user_id = $userId;
        $loanRequest->status = 'STARTED';
        $loanRequest->save();
        return $loanRequest->fresh();
    }

    public function listPurpose(){
        $transactions = [
            'Business cash flow',
            'Business expansion',
            'Car',
            'Credit card debt',
            'Daily Expenses',
            'Debt consolidation',
            'Repay loans to family and friends',
            'Education',
            'Home expenses',
            'Hobbies',
            'Investment',
            'Medical',
            'Utility bills',
            'Renovation',
            'Special occasion',
            'Vacation',
            'Wedding',
            'Others'

        ];
        return response()->json($this->responseHelper->successWithData($transactions), 200);
    }

    public function loanTransaction(){
        $userId = auth()->user()->id;
        $transactions = LoanRequest::with(['offer','transactions'])->where("user_id", $userId)->where("status", "!=", "STARTED");
        if(request()->get('status') && !empty(request()->get('status')) && request()->get('status') != 'all') {
            
            $status = request()->get("status");
            $transactions = $transactions->whereHas("offer", function($q) use($status) {
                $q->where('status', $status);
            });
        } else {
            $transactions = $transactions->get();
        } 
        return response()->json($this->responseHelper->successWithData($transactions), 200);
    }

    public function loanTransactionId($id){
        $userId = auth()->user()->id;
        $transactions = LoanRequest::with(['offer','transactions'])->find($id);

        if (!$transactions) {
            return response()->json($this->responseHelper->errorCustom(422, "Loan Not Found"), 422);
        }

        return response()->json($this->responseHelper->successWithData($transactions), 200);
    }

    public function newLoanRequest(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'loan_amount' => 'required|numeric',
                'loan_duration' => 'required|numeric'
            ]
        );

        if ($validator->fails()) {
            return response()->json($this->responseHelper->errorCustom(422, $validator->errors()->first()), 422);
        }

        $getKyc = KycSessions::where("user_id", auth()->user()->id)->first();
        try {
            $userId = auth()->user()->id;
            $loanRequest = new LoanRequest();
            $loanRequest->user_id = $userId;
            $loanRequest->status = 'STARTED';
            $loanRequest->loan_amount = $request->loan_amount;
            $loanRequest->loan_duration = $request->loan_duration;
            $loanRequest->min_interest_rate = 1;
            $loanRequest->max_interest_rate = 4;
            $loanRequest->interest_rate_type = 'Month';
            $loanRequest->profile = data_get($getKyc, "profile", []);
            $loanRequest->save();
            return response()->json($this->responseHelper->successWithData($loanRequest), 200);
        } catch (\Exception $e) {
            save_error_log($e);
            return response()->json($this->responseHelper->errorCustom(422, "Something went wrong. Please reload the page and try again."), 422);
        }
    }

    public function loanRequestUpdateProfile(Request $request, $id)
    {
        $rules = [];
        $messages = [];
        // rules
        $rules["first_name"]            = "required|string";
        $rules["last_name"]             = "required|string";
        $rules["gender"]                = "required|string";
        $rules["dob"]                   = "required|date_format:m/d/Y";
        $rules["phone"]                 = "required|string";
        $rules["nationality"]           = "required|string";
        $rules["country_of_residence"]  = "required|string";
        $rules["year_in_country"]       = "required|string";
        $rules["address_line_1"]        = "required|string";
        $rules["address_line_2"]        = "required|string";
        $rules["city"]                  = "required|string";
        $rules["zip"]                   = "required|string";
        $rules["twitter"]               = "nullable|string";
        $rules["facebook"]              = "nullable|string";
        $rules["instagram"]             = "nullable|string";
        $rules["tiktok"]                = "nullable|string";
        $rules["education"]             = "required|string";
        $rules["marital_status"]        = "required|string";
        $rules["children"]              = "required|numeric";
        $rules["monthly_income"]        = "required|numeric";
        $rules["source_income"]         = "required|numeric";
        $rules["monthly_liabilities"]   = "nullable|numeric";

        // $messages
        $messages["first_name.required"]            = __("Please enter your full First Name.");
        $messages["last_name.required"]             = __("Please enter your full Last Name.");
        $messages["gender.required"]                = __("Please Choose your Gender.");
        $messages["dob.required"]                   = __("Please Input your Date of Birth.");
        $messages["dob.date_format"]                = __("Enter Date of Birth in this 'mm/dd/yyyy' format.");
        $messages["phone.required"]                 = __("Please Input your Phone Number.");
        $messages["nationality.required"]           = __("Please Input your nationality Number.");
        $messages["country_of_residence.required"]  = __("Please Input your Country of Residence Number.");
        $messages["year_in_country.required"]       = __("Please Input your Year in Country.");
        $messages["address_line_1.required"]        = __("Please Input your Address Line 1.");
        $messages["address_line_2.required"]        = __("Please Input your Address Line 2.");
        $messages["city.required"]                  = __("Please Choose your City.");
        $messages["zip.required"]                   = __("Please Input your Zip Code.");
        $messages["education.required"]             = __("Please Choose your Education.");
        $messages["marital_status.required"]        = __("Please Choose your Marital Status.");
        $messages["children.required"]              = __("Please Choose your Marital Status.");
        $messages["monthly_income.required"]        = __("Please Input your Monthly Income.");
        $messages["source_income.required"]         = __("Please Choose your Source Income.");
        // $messages["monthly_liabilities.required"]   = __("Please Choose your Monthly Liability.");

        $validatedData = request()->validate($rules, $messages);
        $updateData = Arr::only($validatedData, ["first_name","last_name","gender","dob","phone","nationality","country_of_residence","year_in_country","address_line_1","address_line_2","city","zip","twitter","facebook","instagram","tiktok","education","marital_status","children","monthly_income","source_income","monthly_liabilities",]);
        $updateData = array_map("strip_tags_map", $updateData);

        $data = LoanRequest::with(['offer','transactions'])->find($id);
        $getSession = KycSessions::where("user_id", auth()->user()->id)->first();
        if (!$data) {
            return response()->json($this->responseHelper->errorCustom(422, "Loan Not Found"), 422);
        }

        try {
            $data->profile = array_merge(data_get($data, "profile", []), $updateData);
            $data->save();

            $getSession->profile = array_merge(data_get($getSession, "profile", []), $updateData);
            $getSession->save();
            return response()->json($this->responseHelper->successWithData($data), 200);
        } catch (\Exception $e) {
            save_error_log($e);
            return response()->json($this->responseHelper->errorCustom(422, $e->getMessage()), 422);
        }
    }

    public function loanRequestDocument(Request $request, $id)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'utility_bill' => 'file',
                'current_saving' => 'file',
                'latest_payslip' => 'file',
                'other_asset' => 'file',
                'credit_bureau' => 'file',
                'loan_purpose' => 'required|string',
                'bank_balance' => 'nullable',
                'other_liquid_asset' => 'nullable',
                'credit_bureau_queries' => 'nullable',
                'social_proof' => 'nullable',
            ]
        );

        if ($validator->fails()) {
            return response()->json($this->responseHelper->errorCustom(422, $validator->errors()->first()), 422);
        }

        $data = LoanRequest::with(['offer','transactions'])->find($id);

        if (!$data) {
            return response()->json($this->responseHelper->errorCustom(422, "Loan Not Found"), 422);
        }

        try {

            if (isset($request->utility_bill)){
                $file_utility_bill = $request->utility_bill;
                $exten_utility_bill = strtolower($file_utility_bill->getClientOriginalExtension());
                $name_utility_bill = auth()->id() . "_" . "ub_" . $data->id . "." . $exten_utility_bill;
                $file_utility_bill->storeAs("loan-document", $name_utility_bill);
            }
            if (isset($request->current_saving)){
                $file_current_saving = $request->current_saving;
                $exten_current_saving = strtolower($file_current_saving->getClientOriginalExtension());
                $name_current_saving = auth()->id() . "_" . "sv_" . $data->id . "." . $exten_current_saving;
                $file_current_saving->storeAs("loan-document", $name_current_saving);
            }

            if (isset($request->latest_payslip)){
                $file_latest_payslip = $request->latest_payslip;
                $exten_latest_payslip = strtolower($file_latest_payslip->getClientOriginalExtension());
                $name_latest_payslip = auth()->id() . "_" . "lp_" . $data->id . "." . $exten_latest_payslip;
                $file_latest_payslip->storeAs("loan-document", $name_latest_payslip);
            }

            if (isset($request->other_asset)){
                $file_other_asset = $request->other_asset;
                $exten_other_asset = strtolower($file_other_asset->getClientOriginalExtension());
                $name_other_asset = auth()->id() . "_" . "oa_" . $data->id . "." . $exten_other_asset;
                $file_other_asset->storeAs("loan-document", $name_other_asset);
            }

            if (isset($request->credit_bureau)){
                $file_credit_bureau = $request->credit_bureau;
                $exten_credit_bureau = strtolower($file_credit_bureau->getClientOriginalExtension());
                $name_credit_bureau = auth()->id() . "_" . "cb_" . $data->id . "." . $exten_credit_bureau;
                $file_credit_bureau->storeAs("loan-document", $name_credit_bureau);
            }

            $resultFile = [
                "ub" => $name_utility_bill ?? null,
                "sv" => $name_current_saving ?? null,
                "lp" => $name_latest_payslip ?? null,
                "oa" => $name_other_asset ?? null,
                "cb" => $name_credit_bureau ?? null,
                "loan_purpose" => $request->loan_purpose,
                "bank_balance" => $request->bank_balance,
                "other_liquid_asset" => $request->other_liquid_asset,
                "credit_bureau_queries" => $request->credit_bureau_queries,
                "social_proof" => $request->social_proof,
            ];

            $data->documents = $resultFile;
            $data->save();
            return response()->json($this->responseHelper->successWithData($data), 200);
        } catch (\Exception $e) {
            save_error_log($e);
            return response()->json($e->getMessage());
            return response()->json($this->responseHelper->errorCustom(422, "Something went wrong. Please reload the page and try again."), 422);
        }
    }

    public function loanRequestWalletAddress(Request $request, $id)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'wallet_address' => 'required|string'
            ]
        );

        if ($validator->fails()) {
            return response()->json($this->responseHelper->errorCustom(422, $validator->errors()->first()), 422);
        }

        $data = LoanRequest::with(['offer','transactions'])->find($id);

        if (!$data) {
            return response()->json($this->responseHelper->errorCustom(422, "Loan Not Found"), 422);
        }

        try {
            $data->wallet_address = $request->wallet_address;
            $data->save();
            return response()->json($this->responseHelper->successWithData($data), 200);
        } catch (\Exception $e) {
            save_error_log($e);
            return response()->json($this->responseHelper->errorCustom(422, "Something went wrong. Please reload the page and try again."), 422);
        }
    }

    public function loanRequestConfirm(Request $request, $id)
    {
        $data = LoanRequest::with(['offer','transactions'])->find($id);

        if (!$data) {
            return response()->json($this->responseHelper->errorCustom(422, "Loan Not Found"), 422);
        }

        try {
            $data->status = 'Pending Offer';
            $data->save();
            return response()->json($this->responseHelper->successWithData($data), 200);
        } catch (\Exception $e) {
            save_error_log($e);
            return response()->json($this->responseHelper->errorCustom(422, "Something went wrong. Please reload the page and try again."), 422);
        }
    }

    public function loangetOffer(){
        $userId = auth()->user()->id;
        $transactions = LoanOffer::with(['user','loan.transactions'])->where("user_id", $userId);
        
        if(request()->get('status') && !empty(request()->get('status')) && request()->get('status') != 'all') {
            $transactions = $transactions->where('status', request()->get("status"));
        } 
        else {
            $transactions = $transactions->get();
        }

        return response()->json($this->responseHelper->successWithData($transactions), 200);
    }

    public function LoanOfferId($id){
        $userId = auth()->user()->id;
        $transactions = LoanOffer::with(['user','loan.transactions'])->where('loan_id',$id)->first();

        if (!$transactions) {
            return response()->json($this->responseHelper->errorCustom(422, "Offer Not Found"), 422);
        }

        return response()->json($this->responseHelper->successWithData($transactions), 200);
    }

    public function LoanOfferRequest(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'loan_id' => 'required|numeric'
            ]
        );

        if ($validator->fails()) {
            return response()->json($this->responseHelper->errorCustom(422, $validator->errors()->first()), 422);
        }

        $findLoan = loanRequest::with(['offer','transactions'])->find($request->loan_id);

        if (!$findLoan) {
            return response()->json($this->responseHelper->errorCustom(422, "Loan Not Found"), 422);
        }

        try {
            $LoanOffer = new LoanOffer();
            $LoanOffer->user_id = auth()->user()->id;
            $LoanOffer->loan_id = $request->loan_id;
            $LoanOffer->provider_name = 'Sample Provider';
            $LoanOffer->interest_amount =  (2/100) * $findLoan->loan_duration * $findLoan->loan_amount;
            $LoanOffer->interest_rate = 2;
            $LoanOffer->repayment_amount = ((2/100) * $findLoan->loan_duration * $findLoan->loan_amount) + $findLoan->loan_amount;
            $LoanOffer->late_charges = 10;
            $LoanOffer->status = 'STARTED';
            $LoanOffer->save();
            $findLoan->status = 'Offer Received';
            $findLoan->save();
            return response()->json($this->responseHelper->successWithData($LoanOffer), 200);
        } catch (\Exception $e) {
            save_error_log($e);
            return response()->json($this->responseHelper->errorCustom(422, "Something went wrong. Please reload the page and try again."), 422);
        }
    }

    public function LoanStatusRequestUpdate(Request $request, $id)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'status' => 'required|string'
            ]
        );

        if ($validator->fails()) {
            return response()->json($this->responseHelper->errorCustom(422, $validator->errors()->first()), 422);
        }

        $findLoan = LoanRequest::with(['user','offer','transactions'])->find($id);

        if (!$findLoan) {
            return response()->json($this->responseHelper->errorCustom(422, "Loan Not Found"), 422);
        }

        try {
            if($request->status == 'Active' || $request->status == 'ACTIVE'){
                $findLoan->activate_at = Carbon::now();
            }
            $findLoan->status = $request->status;
            $findLoan->save(); 

            return response()->json($this->responseHelper->successWithData($findLoan), 200);
        } catch (\Exception $e) {
            save_error_log($e);
            return response()->json($this->responseHelper->errorCustom(422, $e->getMessage()), 422);
        }
    }
    public function loanOfferRequestUpdate(Request $request, $id)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'status' => 'required|string'
            ]
        );

        if ($validator->fails()) {
            return response()->json($this->responseHelper->errorCustom(422, $validator->errors()->first()), 422);
        }

        $findOffer = LoanOffer::with(['user','loan.transactions'])->find($id);

        if (!$findOffer) {
            return response()->json($this->responseHelper->errorCustom(422, "offer Not Found"), 422);
        }

        try {
            $findOffer->status = $request->status;
            $findOffer->save();

            $findLoan = LoanRequest::find($findOffer->loan_id);
            if($request->status == 'ACCEPT')
            {
                $findLoan->status = 'Pending Test Amount';
            } else {
                $findLoan->status = 'Pending Offer';        
            }
            $findLoan->save();

            return response()->json($this->responseHelper->successWithData($findOffer), 200);
        } catch (\Exception $e) {
            save_error_log($e);
            return response()->json($this->responseHelper->errorCustom(422, "Something went wrong. Please reload the page and try again."), 422);
        }
    }

    public function loanRequestTransaction(Request $request, $id)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'transaction_hash' => 'required'
            ]
        );

        if ($validator->fails()) {
            return response()->json($this->responseHelper->errorCustom(422, $validator->errors()->first()), 422);
        }

        $findLoan = LoanRequest::with(['user','offer','transactions'])->find($id);
        if (!$findLoan) {
            return response()->json($this->responseHelper->errorCustom(422, "Loan Not Found"), 422);
        }

        try {
            $loanTransaction = new loanTransaction();
            $loanTransaction->loan_id = $id;
            $loanTransaction->transaction_hash = $request->transaction_hash;
            $loanTransaction->status = 'Transaction Received';
            $loanTransaction->save();

            $findLoan->status = 'Pending Repayment';
            $findLoan->save();
            return response()->json($this->responseHelper->successWithData($loanTransaction), 200);
        } catch (\Exception $e) {
            save_error_log($e);
            return response()->json($this->responseHelper->errorCustom(422, "Something went wrong. Please reload the page and try again."), 422);
        }
    }

}
