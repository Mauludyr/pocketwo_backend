<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Models\KycQuestion;
use App\Models\UserKycQuestion;
use App\Http\Controllers\Controller;

class KycQuestionController extends Controller
{
    public function getQuestion(){
        $question = KycQuestion::with('answer')->get();
        return response()->json([
            'question' => $question
        ]);
    }

    public function submitQuestion(Request $request){
        $userQuestion = new UserKycQuestion;
        $userQuestion->question_id = $request->question_id;
        $userQuestion->question_answer_id = $request->question_answer_id;
        $userQuestion->score = $request->score;
        $userQuestion->batch = UserKycQuestion::where('user_id')->max('batch') + 1;
        $userQuestion->save();

        return response()->json([
            'message' => 'success',
            'status' => 200,
        ]);
    }
}
