<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\BasicKYC\Models\KycSessions;
use Illuminate\Support\Carbon;

class LoanOffer extends Model
{
    use HasFactory,SoftDeletes;

    protected $fillable = [];

    protected $appends = ['loan_agreement'];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function loan()
    {
        return $this->belongsTo(LoanRequest::class, 'loan_id', 'id');
    }

    public function getLoanAgreementAttribute()
    {
        $userLender = User::find($this->user_id);

        $lender_name = '';
        $lender_address = '';
        if (!$userLender){
            return '';
        }

        if ($userLender->role == 'admin'){
            $userAddress = UserMeta::where('user_id', $userLender->id)->where('meta_key', 'profile_address_line_1')->first();
            $userCountry = UserMeta::where('user_id', $userLender->id)->where('meta_key', 'profile_countrsy')->first();
            $lender_name = $userLender->name;
            $address = !$userAddress ? '' : $userAddress->meta_value;
            $country = !$userCountry ? '' : $userCountry->meta_value;
            $lender_address = $address.' '.$country;
        } else{
            $kycLender = KycSessions::where('user_id', $userLender->id)->first();
            if (!$kycLender){
                return '';
            }
            $lender_name = $kycLender->profile['first_name'].' '.$kycLender->profile['last_name'];
            $lender_address = $kycLender->profile['address_line_1'].' '.$kycLender->profile['city'].' '.$kycLender->profile['zip'];
        }
        $loanBorrower = LoanRequest::find($this->loan_id);
        if (!$loanBorrower){
            return '';
        }
        $userBorrower = User::find($loanBorrower->user_id);
        if (!$userBorrower){
            return '';
        }
        $kycBorrower = KycSessions::where('user_id', $userBorrower->id)->first();
        if (!$kycBorrower){
            return '';
        }
        
        return '<p>
            THIS LOAN AGREEMENT (this “Agreement”) is dated '.Carbon::parse($this->created_at)->format(sys_settings('date_format')).' is made between '.$this->provider_name.', '.$lender_address.' (the “Lender”) OF THE FIRST PART and '.$kycBorrower->profile['first_name'].' '.$kycBorrower->profile['last_name'].', of '. $kycBorrower->profile['address_line_1'].' '.$kycBorrower->profile['city'].' '.$kycBorrower->profile['zip'] .' (the “Borrower”) OF THE SECOND PART (collectively, the “Parties”) IN CONSIDERATION OF the Lender loaning certain cryptocurrency (the “Loan”) to the Borrower, and the Borrower repaying the Loan to the Lender, both parties agree to keep, perform and fulfil the promises and conditions set out in this Agreement. 
            </p>
            <ul>
                <li>Loan Amount & Interest</br>1. The Lender promises to loan USDT '.$this->loan->loan_amount.' (the “Principal Amount”) to the Borrower and the Borrower promises to repay this Principal Amount to the Lender, with interest payable of USDT '.$this->interest_amount.' (the “Interest”).</li>
                <li>Disbursement & Repayment</br>2. The Lender shall disburse the Principal Amount no later than 3 working days from the date of this Agreement. The Borrower shall repay the Principal Amount plus Interest (the “Repayment Amount”), no later than '.$this->loan->loan_duration.' calendar months from the date of full loan disbursement (the “Loan Period”).</li> 
                <li>Default</br>3. Notwithstanding anything to the contrary in this Agreement, if the Borrower fails to repay in full the Repayment Amount within the Loan Period, the Lender may choose to enforce this Agreement to the extent as permitted by local governing laws. Late charges will be calculated at 4.0% per month of outstanding Repayment Amount (“Late Interest”), calculated monthly in advance. For avoidance of doubt, partial repayments will be used to repay Late Interest first, if any, before repaying the Repayment Amount. 
                <li>Governing Law</br>4. This Agreement will be construed in accordance with and governed by the laws of Singapore.</li> 
                <li>Costs</br>5. The Borrower shall be liable for all costs, expenses and expenditures incurred including, without limitation, the complete legal costs of the Lender incurred by enforcing this Agreement as a result of any default by the Borrower.</li>
                <li>Binding Effect</br>6. This Agreement will pass to the benefit of and be binding upon the respective heirs, executors, administrators, successors and permitted assigns of the Borrower and Lender. The Borrower waives presentment for payment, notice of non-payment, protest and notice of protest.</li>
                <li>Amendments</br>7. This Agreement may only be amended or modified by a written instrument executed by both the Borrower and the Lender.</li>
                <li>Severability</br>8. The clauses and paragraphs contained in this Agreement are intended to be read and construed independently of each other. If any term, covenant, condition or provision of this Agreement is held by a court of competent jurisdiction to be invalid, void or unenforceable, it is the parties’ intent that such provision be reduced in scope by the court only to the extent deemed necessary by that court to render the provision reasonable and enforceable and the remainder of the provisions of this Agreement will in no way be affected, impaired or invalidated as a result.</li>
                <li>General Provisions</br>9. Headings are inserted for the convenience of the parties only and are not to be considered when interpreting this Agreement. Words in the singular mean and include the plural and vice versa. Words in the masculine mean and include the feminine and vice versa.</li> 
                <li>Entire Agreement</br>10. This Agreement constitutes the entire agreement between the parties and there are no further items or provisions, either oral or otherwise.</li>
            </ul>';
    }
}
