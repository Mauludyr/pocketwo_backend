<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class KycSession extends Model
{
    protected $table = 'kyc_sessions';

    /**
     * @var string[]
     */
    protected $fillable = [
        'user_id',
        'session',
        'status',
        'method',
        'profile',
        'docs',
        'note',
        'remarks',
        'revised',
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

}
