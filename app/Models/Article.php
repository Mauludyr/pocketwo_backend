<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Article extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'description',
        'file_path',
    ];

    public function categories()
    {
        return $this->hasMany(ArticleCategory::class)->orderBy('id', 'desc')->limit(20);
    }
}
