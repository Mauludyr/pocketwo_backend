<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class LoanTransaction extends Model
{
    use HasFactory,SoftDeletes;

    protected $fillable = ['loan_id','transaction_hash','status'];

    public function loan()
    {
        return $this->belongsTo(LoanRequest::class, 'loan_id', 'id');
    }
}
