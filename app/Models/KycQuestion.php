<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class KycQuestion extends Model
{
    use HasFactory;

    public function answer(){
        return $this->hasMany(KycQuestionAnswer::class,'question_id');
    }

    public function userQuestion(){
        return $this->hasOne(UserKycQuestion::class,'question_id', 'id');
    }
}
