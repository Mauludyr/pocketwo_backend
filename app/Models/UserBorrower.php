<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserBorrower extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = ["occupation_id", "email", "age"];

    public function occupation()
    {
        return $this->belongsTo(Occupation::class, 'occupation_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'email', 'email');
    }
}
