<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserLender extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = ["type_id", "email"];

    public function lenderType()
    {
        return $this->belongsTo(LenderType::class, 'type_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'email', 'email');
    }
}
