<?php

namespace Modules\BasicKYC\RequestFilters;

use Closure;
use App\Helpers\helpers;
use Illuminate\Support\Facades\Auth;

class AllowFilter
{
    public function handle($request, Closure $next)
    {
        if (!feature_enable("kyc")) {
            return abort(404);
        }
        $errors = false;
        $user = auth()->user();
        if (!$user->is_verified) {
            $errors = module_msg_of("email", "alert", "BasicKYC");
        } else {
            if (gss("kyc_disable_request") == "yes") {
                $errors = module_msg_of("disable", "error", "BasicKYC");
            } else {
                if (gss("kyc_profile_complete", "yes") == "yes" && !$user->has_basic) {
                    $errors = module_msg_of("profile", "alert", "BasicKYC");
                }
            }
        }
        if ($errors) {
            if ($request->ajax()) {
                return response()->view("BasicKYC::user.misc.notice", $errors);
            }
            return response()->view("BasicKYC::user.verification-status", $errors);
        }
        return $next($request);
    }
}

?>