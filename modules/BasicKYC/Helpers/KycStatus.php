<?php
/*
 * @ https://EasyToYou.eu - IonCube v11 Decoder Online
 * @ PHP 7.2
 * @ Decoder version: 1.0.4
 * @ Release: 01/09/2021
 */

namespace Modules\BasicKYC\Helpers;

final class KycStatus
{
    const STARTED = "started";
    const RESUBMIT = "resubmit";
    const PENDING = "pending";
    const REJECTED = "rejected";
    const VERIFIED = "verified";
    const FINISHED = "finished";
}

?>