<?php

namespace Modules\BasicKYC\Helpers;

use App\Filters\QueryFilters;

use Modules\BasicKYC\BasicKYCModule;

use Illuminate\Http\Request;
use Illuminate\Support\Str;

class KycFilter extends QueryFilters
{
    public function __construct(Request $request)
    {
        parent::__construct($request);
    }
    public function queryFilter($value)
    {
        $value = Str::upper($value);
        $prefix = config("modules." . BasicKYCModule::SLUG . ".prefix");
        if (Str::startsWith($value, $prefix)) {
            $value = Str::replaceFirst($prefix, "", $value);
            return $this->builder->join("kyc_applicants", "kyc_sessions.user_id", "=", "kyc_applicants.user_id")->where("kyc_applicants.reference", "=", $value);
        }
        if (Str::startsWith($value, config("pocketwo.uid_prefix"))) {
            $value = get_uid($value);
            return $this->builder->where("user_id", $value);
        }
        return $this->builder;
    }
}

?>