<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKycSessionsTable extends Migration
{
    public function up()
    {
        Schema::create("kyc_sessions", function (Blueprint $table) {
            $table->bigIncrements("id");
            $table->bigInteger("user_id")->index();
            $table->string("session")->unique();
            $table->string("status");
            $table->string("method")->nullable();
            $table->longText("profile")->nullable();
            $table->longText("docs")->nullable();
            $table->text("note")->nullable();
            $table->text("remarks")->nullable();
            $table->longText("revised")->nullable();
            $table->text("checked_by")->nullable();
            $table->dateTime("checked_at")->nullable();
            $table->timestamps();
        });
    }
    public function down()
    {
        Schema::dropIfExists("kyc_sessions");
    }
}

?>