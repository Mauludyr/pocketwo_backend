<?php

return [
    "error" => [
        "deposit" => [
            "icon" => "ni-user-check bg-info", 
            "title" => __("Identity verification!"), 
            "notice" => [
                "caption" => __("In order to deposit funds, identity verification is required. Please feel to complete your identity verification."), 
                "note" => "To comply with regulation you will have to go through identity verification.", 
                "class" => ""
            ], 
            "button" => [
                "text" => __("Start Verification"), 
                "url" => route("user.kyc.verify"), 
                "class" => "btn-primary"
            ], 
            "link" => false, 
            "help" => __("Please feel free to contact us if you have any question.")
        ], 
        "withdraw" => [
            "icon" => "ni-user-check bg-info", 
            "title" => __("Identity verification!"), 
            "notice" => [
                "caption" => __("In order to withdraw funds, identity verification is required. Please feel to complete your identity verification."), 
                "note" => "To comply with regulation you will have to go through identity verification.", 
                "class" => ""
            ], 
            "button" => [
                "text" => __("Start Verification"), 
                "url" => route("user.kyc.verify"), 
                "class" => 
                "btn-primary"
            ], 
            "link" => false, 
            "help" => __("Please feel free to contact us if you have any question.")
        ], 
        "disable" => [
            "icon" => "ni-alert bg-warning", 
            "title" => sys_settings("kyc_disable_title", __("Temporarily unavailable!")), 
            "notice" => [
                "caption" => sys_settings("kyc_disable_notice", __("Sorry for any inconvenience. Our identity verification service is not available, please check after sometimes.")), 
                "note" => "", 
                "class" => ""
            ], 
            "button" => [
                "text" => __("Go to Dashboard"), 
                "url" => route("dashboard"), 
                "class" => "btn-primary"
            ], 
            "link" => false, 
            "help" => __("Please feel free to contact us if you have any question.")
        ], 
        "wrong" => [
            "icon" => "ni-alert bg-danger", 
            "title" => __("Sorry, Unable to proceed!"), 
            "notice" => [
                "caption" => __("Sorry, we are unable to proceed your request. Please reload the page and try again."), 
                "note" => __("Error Reference ID :code", [
                    "code" => "0x9101"
                ]), 
                "class" => ""
            ], 
            "button" => [
                "text" => __("Go to Dashboard"), 
                "url" => route("dashboard"), 
                "class" => "btn-primary"
            ], 
            "link" => false, 
            "help" => __("If you continue to having trouble? :Contact or email at :email", [
                "contact" => get_page_link("contact", __("Contact us")), 
                "email" => get_mail_link()
            ])
        ]
    ], 
    "alert" => [
        "email" => [
            "icon" => "ni-mail bg-info", 
            "title" => __("Verify your email address"), 
            "notice" => [
                "caption" => __("Your email address has not been verified! In order to proceed identity verification, you need to verify your email address first."), 
                "note" => "", 
                "class" => ""
            ], 
            "button" => [
                "text" => __("Verify email now"), 
                "url" => route("account.profile"), 
                "class" => "btn-primary"
            ], 
            "link" => false
        ], 
        "profile" => [
            "icon" => "ni-user-alt-fill bg-info", 
            "title" => __("Complete Your Profile"), 
            "notice" => [
                "caption" => __("In order to proceed identity verification, you will need to complete your profile information first."), 
                "note" => "", 
                "class" => ""
            ], 
            "button" => [
                "text" => __("Update profile now"), 
                "url" => route("account.profile"), 
                "class" => "btn-primary"], 
                "link" => false, 
                "help" => __("Please feel free to contact us if you have any question.")
            ]
        ], 
        "status" => [
            "pending" => [
                "icon" => "ni-shield-half bg-primary", 
                "title" => __("Identity verification under review!"), 
                "notice" => [
                    "caption" => __("You have successfully submitted your documents for identity verification. One of our team member will review your documents and will confirm you its progress via email."), 
                    "note" => "We are still working on it and our verification process may take few days.", 
                    "class" => ""
                ], 
                "button" => [
                    "text" => __("Go to Dashboard"), 
                    "url" => route("dashboard"), 
                    "class" => "btn-primary d-none"
                ], 
                "link" => [
                    "text" => has_route("user.investment.plans") ? __("Check our available plans") : "", 
                    "url" => has_route("user.investment.plans") ? route("user.investment.plans") : "", 
                    "class" => "link-primary d-none"
                ], 
                "help" => __("Please feel free to contact if you need any further information.")
            ], 
            "rejected" => [
                "icon" => "ni-user-cross bg-danger", 
                "title" => __("Identity verification failed!"), 
                "notice" => [
                    "caption" => __("Sorry, we are unable to verify your identity. In our verification process, we found that your information is not valid."), 
                    "note" => "You are no longer allow to submit your documents. In case you want to re-submit your valid documents, please contact us.", 
                    "class" => ""
                ], 
                "button" => [
                    "text" => __("Go to Dashboard"), 
                    "url" => route("dashboard"), 
                    "class" => "btn-primary d-none"
                ], 
                "link" => false, 
                "help" => __("Please feel free to contact if you need any further information.")
            ], 
            "verified" => [
                "icon" => "ni-user-check bg-success", 
                "title" => __("Identity verified successfully!"), 
                "notice" => [
                    "caption" => __("Thanks for submitting your valid documents. One of our team member successfully reviewed your documents and has been verified."), 
                    "note" => "You can use our platform without any limitation.", 
                    "class" => "sm"
                ], 
                "button" => [
                    "text" => __("Go to Dashboard"), 
                    "url" => route("dashboard"), 
                    "class" => "btn-primary"
                ], 
                "link" => [
                    "text" => has_route("user.investment.plans") ? __("Check our available plans") : "", 
                    "url" => has_route("user.investment.plans") ? route("user.investment.plans") : "", 
                    "class" => "link-primary d-none"
                ], 
                "help" => __("Please feel free to contact if you need any further information.")
            ], 
            "success" => [
                "icon" => "ni-check bg-success", 
                "title" => __("You have successfully completed!"), 
                "notice" => [
                    "caption" => __("Congrats, you have successfully submitted all the documents for verification. One of our team member will review your documents and you will be notified via email for its progress."), 
                    "note" => "Our verification process may take 3-5 business days.", 
                    "class" => ""
                ], 
                "button" => [
                    "text" => __("Go to Dashboard"), 
                    "url" => route("dashboard"), 
                    "class" => "btn-primary d-none"
                ], 
                "link" => [
                    "text" => has_route("user.investment.plans") ? __("Check our available plans") : "", 
                    "url" => has_route("user.investment.plans") ? route("user.investment.plans") : "", 
                    "class" => "link-primary d-none"
                ], 
                "help" => __("Please feel free to contact if you need any further information.")
            ]
        ]
    ];

?>