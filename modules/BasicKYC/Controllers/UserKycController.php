<?php

namespace Modules\BasicKYC\Controllers;

use Modules\BasicKYC\BasicKYCModule;
use Modules\BasicKYC\Helpers\ImportSettings;
use App\Jobs\ProcessEmail;
use App\Traits\WrapInTransaction;
use App\Models\User;
use App\Models\UserMeta;
use App\Models\Setting;
use App\Http\Controllers\Controller;
use App\Helpers\ResponseHelper;

use Modules\BasicKYC\Helpers\KycStatus;
use Modules\BasicKYC\Helpers\KycSessionStatus;

use Modules\BasicKYC\Models\KycSessions;
use Modules\BasicKYC\Models\KycApplicants;

use Carbon\Carbon;
use Illuminate\Support\Str;
use Illuminate\Support\Arr;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\ValidationException;

class UserKycController extends Controller
{
    private $kycModule = NULL;
    private $pathTemp = NULL;
    private $pathSave = NULL;
    private $docKeys = NULL;

    public function __construct(BasicKYCModule $module)
    {
        $this->kycModule = $module;
        $this->pathTemp = "kyc-temp";
        $this->pathSave = "kyc";
        $this->docKeys = ["main", "front", "back", "proof", "bs", "ub"];
        $this->responseHelper = new ResponseHelper;
    }

    public function verification()
    {
        $userId = auth()->user()->id;
        $getApplicant = KycApplicants::where("user_id", $userId)->first();
        $getSession = KycSessions::started()->where("user_id", $userId)->first();
        $status = !blank($getSession) ? data_get($getSession, "status") : "none";
        $kycStatus = !blank($getApplicant) ? data_get($getApplicant, "status") : "new";
        $additional = $document = $basic = false;
        if (!blank($getSession)) {
            $basic = $this->isProfileInfoExist($this->getBasicInfo());
            $mainDoc = $getSession->document("main");
            $document = $this->hasDocsFiles($mainDoc);
            $both = $this->docsAlter("req");
            $proof = data_get($getSession, "docs.proof", []);
            if (!empty($proof)) {
                if (!$both && 1 <= count($proof)) {
                    $additional = true;
                } else {
                    if ($both && count($proof) == 2) {
                        $additional = true;
                    }
                }
            }
        }
        $data = [
            "resubmit" => $kycStatus == KycStatus::RESUBMIT ? true : false, 
            "basic" => $basic, 
            "document" => $document, 
            "additional" => $additional, 
            "status" => $status
        ];
        return response()->json($this->responseHelper->successWithData($data), 200);
        
    }
    public function proceedNext(Request $request)
    {
        dd('asdasd');
        $userId = auth()->user()->id;
        $getApplicant = KycApplicants::where("user_id", $userId)->first();
        $getSession = KycSessions::started()->where("user_id", $userId)->first();
        if (blank($getSession)) {
            return $this->basicInfo();
        }
        if (!blank($getSession)) {
            $isBasic = Session::get("basic_complete", "no");
            if (!$this->isProfileInfoExist($this->getBasicInfo()) || $isBasic == "no") {
                return $this->basicInfo();
            }
            $mainDoc = $getSession->document("main");
            if (blank($mainDoc) || !data_get($mainDoc, "files") || !data_get($mainDoc, "meta")) {
                return $this->documents();
            }
            $bs = $this->docsAlter("bs");
            $ub = $this->docsAlter("ub");
            $both = $this->docsAlter("req");
            $bsDoc = $getSession->document("bs");
            $ubDoc = $getSession->document("ub");
            $proof = data_get($getSession, "docs.proof", []);
            if (empty($proof) || $both && count($proof) < 2) {
                return $this->additional();
            }
            if ($both && (!data_get($bsDoc, "files") || !data_get($ubDoc, "files")) || $bs && $ub && !$both && !data_get($bsDoc, "files") && !data_get($ubDoc, "files") || $bs && !$ub && !data_get($bsDoc, "files") || $ub && !$bs && !data_get($ubDoc, "files")) {
                return $this->additional();
            }
            return $this->finalPreview();
        }
        return response()->json($this->responseHelper->errorCustom(422, "Sorry, we are unable to proceed your request. Please reload the page and try again."), 422);
    }
    private function finalPreview()
    {
        $userId = auth()->user()->id;
        $getSession = KycSessions::started()->where("user_id", $userId)->first();
        if (!blank($getSession)) {
            $main = $getSession->document("main");
            $bs = $getSession->document("bs");
            $ub = $getSession->document("ub");
            if (!$this->isProfileInfoExist($getSession->profile)) {
                return $this->basicInfo();
            }
            if (!$this->hasDocsFiles($main)) {
                return $this->documents();
            }
            $session_id = data_get($getSession, "session");

            $data = [
                "data" => $getSession, 
                "main" => $main, 
                "bs" => $bs, 
                "ub" => $ub, 
                "session" => $session_id
            ];
            return response()->json($this->responseHelper->successWithData($data), 200);
        }
        return response()->json($this->responseHelper->errorCustom(422, "Something went wrong. Please reload the page and try again."), 422);
    }
    public function submitKyc(Request $request)
    {
        $sessionHash = $request->get("identity");
        if (empty($sessionHash)) {
            return response()->json($this->responseHelper->errorCustom(422, "Something went wrong. Please reload the page and try again."), 422);
        }

        $userId = auth()->user()->id;
        $getSession = KycSessions::started()->where("user_id", $userId)->first();
        if (!blank($getSession) && data_get($getSession, "session") == get_hash($sessionHash)) {
            $stopSubmit = false;
            if (empty($getSession->docs) || !$this->isProfileInfoExist($getSession->profile) || !$this->hasDocsFiles($getSession->document("main"))) {
                $stopSubmit = true;
            }
            if (in_array("bs", data_get($getSession, "docs.proof", [])) && !$this->hasDocsFiles($getSession->document("bs"))) {
                $stopSubmit = true;
            }
            if (in_array("ub", data_get($getSession, "docs.proof", [])) && !$this->hasDocsFiles($getSession->document("ub"))) {
                $stopSubmit = true;
            }
            if ($stopSubmit) {
                return response()->json($this->responseHelper->errorCustom(422, "Something went wrong. Please reload the page and try again."), 422);
            }

            try {
                return $this->wrapInTransaction(function ($getSession, $userId) {
                    $user = User::findOrFail(auth()->user()->id);
                    $this->moveSubmittedFiles($getSession);
                    $getKycApplicant = KycApplicants::where("user_id", $userId)->first();
                    $getKycApplicant->status = KycStatus::PENDING;
                    $getKycApplicant->save();
                    $getSession->created_at = Carbon::now();
                    $getSession->status = KycSessionStatus::COMPLETED;
                    $getSession->save();
                    UserMeta::updateOrCreate(["user_id" => $userId, "meta_key" => "kyc_verification"], ["meta_value" => "pending"]);
                    Session::put("basic_complete", "no");
                    $success = module_msg_of("success", "status", "BasicKYC");
                    ProcessEmail::dispatch('kyc-submission-user', $user);
                    ProcessEmail::dispatch('kyc-submission-admin', $user);
                    return response()->json($this->responseHelper->successWithData($success), 200);
                }, $getSession, $userId);
            } catch (\Exception $e) {
                save_error_log($e);
                return response()->json($this->responseHelper->errorCustom(422, "Sorry, we are unable to proceed your request."), 422);
            }
        }
        return response()->json($this->responseHelper->errorCustom(422, "Something went wrong. Please reload the page and try again."), 422);
    }
    public function cancelKyc()
    {
        $getSession = KycSessions::started()->where("user_id", auth()->user()->id)->first();
        $this->removeSubmittedFiles($getSession);
        Session::put("basic_complete", "no");
        return response()->json($this->responseHelper->successWithoutData('KYC has been cancelled'), 200);
    }
    private function isProfileInfoExist($profileData)
    {
        $profileFields = Arr::get(gss("kyc_fields"), "profile");
        foreach ($profileFields as $key => $item) {
            if (Arr::get($item, "show") == "yes" && Arr::get($item, "req") == "yes") {
                switch ($key) {
                    case "address":
                        if (!Arr::get($profileData, "address_line_1") || !Arr::get($profileData, "state")) {
                            return false;
                        }
                        break;
                    case "country":
                        if (!Arr::get($profileData, "country")) {
                            return false;
                        }
                        break;
                    case "nationality":
                        if (!Arr::get($profileData, "nationality")) {
                            return false;
                        }
                        break;
                    case "gender":
                        if (!Arr::get($profileData, "gender")) {
                            return false;
                        }
                        break;
                    case "phone":
                        if (!Arr::get($profileData, "phone")) {
                            return false;
                        }
                        break;
                    case "dob":
                        if (!Arr::get($profileData, "dob")) {
                            return false;
                        }
                        break;
                    case "name":
                        if (!Arr::get($profileData, "name")) {
                            return false;
                        }
                        break;
                }
            }
        }
        return true;
    }
    private function getBasicInfo()
    {
        $userId = auth()->user()->id;
        $userMeta = UserMeta::where("user_id", $userId)->pluck("meta_value", "meta_key")->toArray();
        if (!empty($userMeta)) {
            $userMeta = array_filter($userMeta);
        }
        $kycProfile = KycSessions::started()->where("user_id", $userId)->first();
        if (!empty($kycProfile["profile"])) {
            $kycProfile = array_filter($kycProfile["profile"]);
        }
        $nationality = Arr::get($userMeta, "profile_nationality") == "same" ? Arr::get($userMeta, "profile_country") : Arr::get($userMeta, "profile_nationality");
        return response()->json($this->responseHelper->successWithData(["name" => Arr::get($kycProfile, "name", auth()->user()->name), "phone" => Arr::get($kycProfile, "phone", Arr::get($userMeta, "profile_phone")), "dob" => Arr::get($kycProfile, "dob", Arr::get($userMeta, "profile_dob")), "gender" => Arr::get($kycProfile, "gender", Arr::get($userMeta, "profile_gender")), "nationality" => Arr::get($kycProfile, "nationality", $nationality), "address_line_1" => Arr::get($kycProfile, "address_line_1", Arr::get($userMeta, "profile_address_line_1")), "address_line_2" => Arr::get($kycProfile, "address_line_2", Arr::get($userMeta, "profile_address_line_2")), "city" => Arr::get($kycProfile, "city", Arr::get($userMeta, "profile_city")), "state" => Arr::get($kycProfile, "state", Arr::get($userMeta, "profile_state")), "zip" => Arr::get($kycProfile, "zip", Arr::get($userMeta, "profile_zip")), "country" => Arr::get($kycProfile, "country", Arr::get($userMeta, "profile_country"))]), 200);
    }
    public function basicInfo()
    {
        $userId = auth()->user()->id;
        $kycApplicant = KycApplicants::where("user_id", $userId)->first();
        $getSession = KycSessions::started()->where("user_id", $userId)->first();
        $basicInfo = $this->getBasicInfo();
        if (blank($kycApplicant)) {
            $this->addNewApplicant();
        }
        if (blank($getSession)) {
            $addSession = $this->addNewSession($basicInfo);
            $getSession = $addSession;
        }
        $session_id = data_get($getSession, "session");
        if (gss("kyc_preview_quick", "yes") == "yes" && $this->isProfileInfoExist($basicInfo)) {
            Session::put("basic_complete", "yes");
            return response()->json($this->responseHelper->successWithData(["basicInfo" => $basicInfo, "session" => $session_id]), 200);
        }
        $countries = filtered_countries();
        return response()->json($this->responseHelper->successWithData(["basicInfo" => $basicInfo, "countries" => $countries, "session" => $session_id]), 200);
    }
    public function basicInfoForm(Request $request)
    {
        $getSession = KycSessions::started()->where("user_id", auth()->user()->id)->first();
        $session_id = data_get($getSession, "session");
        $countries = filtered_countries();
        return response()->json($this->responseHelper->successWithData(["session" => $session_id, "basicInfo" => $this->getBasicInfo(), "countries" => $countries]), 200);
    }
    public function basicInfoUpdate(Request $request)
    {
        $rules = [];
        $messages = [];
        $settings = data_get(gss("kyc_fields"), "profile");
        $redirect = data_get($settings, "address.show") == "yes" ? true : false;
        $rules["name"] = "required|string|max:190";
        $messages["name.required"] = __("Please enter your full name.");
        if (data_get($settings, "phone.show") == "yes") {
            $rules["phone"] = data_get($settings, "phone.req") == "no" ? "nullable|string" : "required|string|max:50";
            $messages["phone.required"] = __("Please enter your phone number.");
        }
        if (data_get($settings, "dob.show") == "yes") {
            $rules["dob"] = data_get($settings, "dob.req") == "no" ? "nullable|date_format:m/d/Y" : "required|date_format:m/d/Y";
            $messages["dob.required"] = __("Please enter your date of birth.");
            $messages["dob.date_format"] = __("Enter date of birth in this 'mm/dd/yyyy' format.");
        }
        if (data_get($settings, "gender.show") == "yes") {
            $rules["gender"] = data_get($settings, "gender.req") == "no" ? "nullable|string|max:10" : "required|string|max:10";
            $messages["gender.required"] = __("Please select your gender.");
        }
        if (data_get($settings, "nationality.show") == "yes") {
            $rules["nationality"] = data_get($settings, "nationality.req") == "no" ? "nullable|string|max:50" : "required|string|max:50";
            $messages["nationality.required"] = __("Please select your nationality.");
        }
        if (data_get($settings, "address.show") == "no") {
            $rules["country"] = "required|string|max:50";
            $messages["country.required"] = __("Please select country of residence.");
        }
        $validatedData = request()->validate($rules, $messages);
        $updateData = Arr::only($validatedData, ["name", "phone", "dob", "gender", "nationality", "country"]);
        $updateData = array_map("strip_tags_map", $updateData);
        $userId = auth()->user()->id;
        $getSession = KycSessions::started()->where("user_id", $userId)->first();
        if (!blank($getSession) && data_get($getSession, "session") == get_hash($request->get("identity"))) {
            try {
                return $this->wrapInTransaction(function ($getSession, $updateData, $redirect) {
                    $getSession->profile = array_merge(data_get($getSession, "profile", []), $updateData);
                    $getSession->save();
                    Session::put("basic_complete", "yes");
                    $session_id = data_get($getSession, "session");
                    $countries = filtered_countries();
                    if ($redirect) {
                        return response()->json($this->responseHelper->successWithData(["basicInfo" => $this->getBasicInfo(), "countries" => $countries, "session" => $session_id]), 200);
                    }
                    return $this->documents();
                }, $getSession, $updateData, $redirect);
            } catch (\Exception $e) {
                save_error_log($e);
                return response()->json($this->responseHelper->errorCustom(422, "Sorry, we are unable to proceed your request."), 422);
            }
        }
        return response()->json($this->responseHelper->errorCustom(422, "Something went wrong. Please reload the page and try again."), 422);
    }
    public function basicAddressUpdate(Request $request)
    {
        $rules = [];
        $messages = [];
        $require = data_get(gss("kyc_fields"), "profile.address.req") == "yes" ? true : false;
        $rules["address_line_1"] = $require ? "required|string|max:100" : "nullable|string|max:100";
        $rules["address_line_2"] = "nullable|string|max:100";
        $rules["city"] = $require ? "required|string|max:50" : "nullable|string|max:100";
        $rules["state"] = "nullable|string|max:50";
        $rules["zip"] = "nullable|string|max:20";
        $rules["country"] = "required|string|max:50";
        $messages["address_line_1.required"] = __("Please enter your valid address.");
        $messages["city.required"] = __("Please enter your city name.");
        $messages["country.required"] = __("Please select your country name.");
        $validatedData = request()->validate($rules, $messages);
        $updateData = Arr::only($validatedData, ["address_line_1", "address_line_2", "city", "state", "zip", "country"]);
        $updateData = array_map("strip_tags_map", $updateData);
        $userId = auth()->user()->id;
        $getSession = KycSessions::started()->where("user_id", $userId)->first();
        if (!blank($getSession) && data_get($getSession, "session") == get_hash($request->get("identity"))) {
            try {
                return $this->wrapInTransaction(function ($getSession, $updateData) {
                    $getSession->profile = array_merge(data_get($getSession, "profile", []), $updateData);
                    $getSession->save();
                    return $this->documents();
                }, $getSession, $updateData);
            } catch (\Exception $e) {
                save_error_log($e);
                return response()->json($this->responseHelper->errorCustom(422, "Sorry, we are unable to proceed your request."), 422);
            }
        }
        return response()->json($this->responseHelper->errorCustom(422, "Something went wrong. Please reload the page and try again."), 422);
    }
    public function documents()
    {
        $getSession = KycSessions::started()->where("user_id", auth()->user()->id)->first();
        if (!blank($getSession)) {
            $mainDoc = $getSession->document("main");
            $countries = filtered_countries();
            $session_id = data_get($getSession, "session");
            $this->fileCleanup($getSession, "document");
            return response()->json($this->responseHelper->successWithData(["userDoc" => $mainDoc, "countries" => $countries, "session" => $session_id]), 200);
        }
        return response()->json($this->responseHelper->errorCustom(422, "Something went wrong. Please reload the page and try again."), 422);
    }
    public function documentsUpdate(Request $request)
    {
        $fields = data_get(gss("kyc_docs"), "field");
        $rules = ["type" => "required|in:pp,nid,dvl", "country" => "required|string|max:50"];
        $messages = ["type.in" => __("Please select a valid document type."), "type.required" => __("Please select the type of document."), "country.required" => __("Please select the issued by country.")];
        if (data_get($fields, "id.show") == "on") {
            $rules["number"] = data_get($fields, "id.req") == "on" ? "required|string|max:50" : "nullable|string|max:50";
            $messages["number.required"] = __("Please enter your document number.");
        }
        if (data_get($fields, "issue.show") == "on") {
            $rules["issue"] = data_get($fields, "issue.req") == "on" ? "required|date_format:m/d/Y" : "nullable|date_format:m/d/Y";
            $messages["issue.required"] = __("Please enter your document issue date.");
            $messages["issue.date_format"] = __("Enter issue date in this 'mm/dd/yyyy' format.");
        }
        if (data_get($fields, "expiry.show") == "on") {
            $rules["expiry"] = data_get($fields, "expiry.req") == "on" ? "required|date_format:m/d/Y" : "nullable|date_format:m/d/Y";
            $messages["expiry.required"] = __("Please enter your document expiry date.");
            $messages["expiry.date_format"] = __("Enter expiry date in this 'mm/dd/yyyy' format.");
        }
        $validatedData = $request->validate($rules, $messages);
        $docType = data_get($validatedData, "type");
        $docMeta = Arr::only($validatedData, ["country", "number", "issue", "expiry"]);
        $docMeta = array_map("strip_tags_map", $docMeta);
        $userId = auth()->user()->id;
        $getSession = KycSessions::started()->where("user_id", $userId)->first();
        if (!blank($getSession) && data_get($getSession, "session") == get_hash($request->get("identity"))) {
            try {
                $mainDoc = $getSession->document("main");
                $mainFiles = data_get($mainDoc, "files", []);
                $document = short_to_docs($docType);
                if (!blank($mainDoc)) {
                    $this->removeExistFiles($mainFiles);
                    $mainDoc->type = $docType;
                    $mainDoc->meta = $docMeta;
                    $mainDoc->files = [];
                    $mainDoc->save();
                } else {
                    $kycDocs = new \Modules\BasicKYC\Models\KycDocs();
                    $kycDocs->session_id = $getSession->id;
                    $kycDocs->user_id = $getSession->user_id;
                    $kycDocs->type = $docType;
                    $kycDocs->meta = $docMeta;
                    $kycDocs->save();
                    $mainDoc = $kycDocs->fresh();
                }
                $session_id = data_get($getSession, "session");
                $this->fileCleanup($getSession, "document");
                Session::put("main_files", []);
                return response()->json($this->responseHelper->successWithData(["doc" => $docType, "document" => $document, "session" => $session_id]), 200);
            } catch (\Exception $e) {
                save_error_log($e);
                return response()->json($this->responseHelper->errorCustom(422, "Sorry, we are unable to proceed your request."), 422);
            }
        }
        return response()->json($this->responseHelper->errorCustom(422, "Something went wrong. Please reload the page and try again."), 422);
    }
    public function documentsUploadUpdate(Request $request)
    {
        $userId = auth()->user()->id;
        $getSession = KycSessions::started()->where("user_id", $userId)->first();
        $kycDocs = $getSession->document("main");
        if (blank($getSession) || blank($kycDocs) || !$this->docsMain("opt")) {
            return response()->json($this->responseHelper->errorCustom(422, "Something went wrong. Please reload the page and try again."), 422);
        }
        $docFiles = Session::get("main_files", []);
        $docType = $request->get("docstype");
        $userDoc = data_get($kycDocs, "type");
        if ($docType != $userDoc) {
            return response()->json($this->responseHelper->errorCustom(422, "Something went wrong. Please reload the page and try again."), 422);
        }
        if (empty($docFiles)) {
            return response()->json($this->responseHelper->errorCustom(422, "Please upload all the necessary documents."), 422);
        }
        $rules = ["docstype" => "required|in:pp,nid,dvl"];
        $messages = ["docstype.*" => __("Please select a valid document type.")];
        if ($userDoc == "pp") {
            $rules["main"] = "required|string|min:8|max:10";
            $messages["main.*"] = __("Please upload the :document.", ["document" => __(strtolower(short_to_docs($userDoc)))]);
        }
        if ($userDoc == "nid" || $userDoc == "dvl") {
            $rules["front"] = "required|string|min:8|max:10";
            $rules["back"] = "required|string|min:8|max:10";
            $messages["front.*"] = __("Please upload the :part of :document.", ["part" => __("front"), "document" => __(strtolower(short_to_docs($userDoc)))]);
            $messages["back.*"] = __("Please upload the :part of :document.", ["part" => __("back"), "document" => __(strtolower(short_to_docs($userDoc)))]);
        }
        if ($this->docsMain("proof")) {
            $rules["proof"] = "required|string|min:8|max:10";
            $messages["proof.*"] = __("Please upload the :document.", ["document" => __(strtolower(short_to_docs("proof")))]);
        }
        $input = $request->validate($rules, $messages);
        if (!blank($kycDocs)) {
            $fileData = [];
            $docs = Arr::except($input, ["docstype"]);
            $type = data_get($kycDocs, "type");
            foreach ($docs as $key => $hash) {
                $file = data_get($docFiles, $key . "." . $hash);
                if (empty($file) || !Storage::exists($this->pathTemp . "/" . $file)) {
                    if ($key == "main") {
                        $error = __("Please upload the :document.", ["document" => __(short_to_docs(strtolower($type)))]);
                    } else {
                        if ($key == "proof") {
                            $error = __("Take a selfie with :document & upload.", ["document" => __(short_to_docs(strtolower($type)))]);
                        } else {
                            $error = __("Upload the :part copy of :document.", ["part" => __($key), "document" => __(short_to_docs(strtolower($type)))]);
                        }
                    }
                    return response()->json($this->responseHelper->errorCustom(422, $error), 422);
                }
                $fileData[$key] = $file;
            }
            if (!empty($fileData)) {
                $kycDocs->files = $fileData;
                $kycDocs->save();
                $getSession->docs = array_merge(data_get($getSession, "docs", []), ["main" => $type]);
                $getSession->save();
                return $this->additional();
            }
            $getSession->docs = array_merge(data_get($getSession, "docs", []), ["main" => NULL]);
            $getSession->save();
            return response()->json($this->responseHelper->errorCustom(422, "Please upload all the documents for verification."), 422);
        } else {
            return response()->json($this->responseHelper->errorCustom(422, "Something went wrong. Please reload the page and try again."), 422);
        }
    }
    public function additional()
    {
        $userId = auth()->user()->id;
        $getSession = KycSessions::started()->where("user_id", $userId)->first();
        if (!blank($getSession)) {
            if ($this->docsAlter("opt")) {
                $this->fileCleanup($getSession, "additional");
                Session::put("proof_files", []);
                $getBsDoc = $getSession->document("bs");
                $getUbDoc = $getSession->document("ub");
                $required = $this->docsAlter("req");
                $session_id = data_get($getSession, "session");
                return response()->json($this->responseHelper->successWithData(["getBsDoc" => $getBsDoc, "getUbDoc" => $getUbDoc, "bothRequire" => $required, "session" => $session_id]), 200);
            }
            return $this->finalPreview();
        }
        return response()->json($this->responseHelper->errorCustom(422, "Something went wrong. Please reload the page and try again."), 422);
    }
    public function additionalUpdate(Request $request)
    {
        $userId = auth()->user()->id;
        $getSession = KycSessions::started()->where("user_id", $userId)->first();
        if (!blank($getSession)) {
            $docFiles = Session::get("proof_files", []);
            if (empty($docFiles)) {
                return response()->json($this->responseHelper->errorCustom(422, "Please upload all the necessary documents."), 422);
            }
            $bs = $this->docsAlter("bs");
            $ub = $this->docsAlter("ub");
            $both = $this->docsAlter("req");
            $bsdoc = strtolower(short_to_docs("bs"));
            $ubdoc = strtolower(short_to_docs("ub"));
            $rules = $messages = [];
            if ($bs) {
                if ($both) {
                    $rules["bs"] = "required|string|min:8|max:10";
                    $messages["bs.*"] = __("Please upload the :document.", ["document" => __($bsdoc)]);
                } else {
                    $rules["bs"] = "nullable|string|min:8|max:10";
                    $messages["bs.*"] = __("Please upload the :document.", ["document" => __($bsdoc)]);
                }
            }
            if ($ub) {
                if ($both) {
                    $rules["ub"] = "required|string|min:8|max:10";
                    $messages["ub.*"] = __("Please upload the :document.", ["document" => __($ubdoc)]);
                } else {
                    $rules["ub"] = "nullable|string|min:8|max:10";
                    $messages["ub.*"] = __("Please upload the :document.", ["document" => __($ubdoc)]);
                }
            }
            $input = $request->validate($rules, $messages);
            $docs = array_filter(Arr::only($input, ["bs", "ub"]));
            $bsFile = data_get($docFiles, "bs." . Arr::get($docs, "bs", "0"));
            $ubFile = data_get($docFiles, "ub." . Arr::get($docs, "ub", "0"));
            if ($both) {
                if (!$bsFile && !$ubFile) {
                    return response()->json($this->responseHelper->errorCustom(422, "Upload both bank statement or utility bill."), 422);
                }
                if (!$bsFile) {
                    return response()->json($this->responseHelper->errorCustom(422, "Please upload the :document.", ["document" => __($bsdoc)]), 422);
                }
                if (!$ubFile) {
                    return response()->json($this->responseHelper->errorCustom(422, "Please upload the :document.", ["document" => __($ubdoc)]), 422);
                }
            }
            if ($bs && $ub && !$both && !$bsFile && !$ubFile) {
                return response()->json($this->responseHelper->errorCustom(422, "Upload either bank statement or utility bill."), 422);
            }
            if ($bs && !$ub && !$bsFile) {
                return response()->json($this->responseHelper->errorCustom(422, "Please upload the :document.", ["document" => __($bsdoc)]), 422);
            }
            if ($ub && !$bs && !$ubFile) {
                return response()->json($this->responseHelper->errorCustom(422, "Please upload the :document.", ["document" => __($ubdoc)]), 422);
            }
            try {
                return $this->wrapInTransaction(function ($docs, $docFiles, $getSession) {
                    if (!empty($docs) && is_array($docs)) {
                        $bs_ub = [];
                        $isBs = $isUb = false;
                        foreach ($docs as $key => $hash) {
                            $file = data_get($docFiles, $key . "." . $hash);
                            if (!empty($file) && Storage::exists($this->pathTemp . "/" . $file)) {
                                $proofDoc = $getSession->document($key);
                                $this->addOrUpdateProofDoc($proofDoc, $key, $file, $getSession);
                                if ($key == "bs") {
                                    $isBs = true;
                                }
                                if ($key == "ub") {
                                    $isUb = true;
                                }
                            }
                        }
                        if ($isBs) {
                            $bs_ub[] = "bs";
                        }
                        if ($isUb) {
                            $bs_ub[] = "ub";
                        }
                        if (!empty($bs_ub)) {
                            $getSession->docs = array_merge(data_get($getSession, "docs", []), ["proof" => $bs_ub]);
                            $getSession->save();
                        } else {
                            $getSession->docs = array_merge(data_get($getSession, "docs", []), ["proof" => []]);
                            $getSession->save();
                        }
                    }
                    Session::put("proof_files", []);
                    return $this->finalPreview();
                }, $docs, $docFiles, $getSession);
            } catch (\Exception $e) {
                save_error_log($e);
                return response()->json($this->responseHelper->errorCustom(422, "Something went wrong. Please reload the page and try again."), 422);
            }
        }
        return response()->json($this->responseHelper->errorCustom(422, "Something went wrong. Please reload the page and try again."), 422);
    }
    private function addOrUpdateProofDoc($proof, $key, $file, $session)
    {
        if (!empty($key) && !empty($file)) {
            if (!blank($proof)) {
                $files = data_get($proof, "files", []);
                $this->removeExistFiles($files);
                $proof->type = $key;
                $proof->files = ["main" => $file];
                $proof->save();
            } else {
                $kycDoc = new \Modules\BasicKYC\Models\KycDocs();
                $kycDoc->session_id = $session->id;
                $kycDoc->user_id = $session->user_id;
                $kycDoc->type = $key;
                $kycDoc->files = ["main" => $file];
                $kycDoc->save();
            }
        }
    }
    public function handleFiles(Request $request)
    {
        $input = $request->validate(["doc" => "required|in:main,front,back,proof,bs,ub", "action" => "in:store,delete", "document" => "file|mimetypes:image/jpg,image/png,image/jpeg|max:5200"], ["doc.*" => __("Invalid action taken."), "action.in" => __("Invalid action taken."), "document.max" => __("File exceeds the maximum upload file size of 5 MB."), "document.file" => __("Please select a valid file type and try once again."), "document.mimetypes" => __("Invalid file type, only JPG and PNG files are allowed."), "document.uploaded" => __("Unable to upload file due to technical issues.")]);
        $uploaded = in_array($input["doc"], ["bs", "ub"]) ? "proof_files" : "main_files";
        $paths = Session::get($uploaded, []);
        if ($input["action"] == "store") {
            try {
                $file = $request->document;
                $docType = $request->doc;
                if (empty($file) || empty($docType) || !in_array($docType, $this->docKeys)) {
                    return response()->json($this->responseHelper->errorCustom(81405, "Invalid action taken."), 81405);
                }
                $hash = cipher(now()->timestamp);
                $exten = strtolower($file->getClientOriginalExtension());
                $name = auth()->id() . "_" . $docType . "_" . $hash . "." . $exten;
                $paths[$docType][$hash] = $name;
                $file->storeAs($this->pathTemp, $name);
                Session::put($uploaded, $paths);
                return response()->json($this->responseHelper->errorCustom(422, "File has been uploaded."), "hash" => $hash]), 422);
            } catch (\Exception $e) {
                save_msg_log($e->getMessage(), "info");
                return response()->json($this->responseHelper->errorCustom(81402, "Unable to upload file."), 81402);
            }
        }
        if ($input["action"] == "delete") {
            if (empty($request->doc) || empty($request->hash) || !in_array($request->doc, $this->docKeys)) {
                return response()->json($this->responseHelper->errorCustom(82405, "Invalid action taken."), 82405);
            }
            try {
                if (!empty($paths[$request->doc][$request->hash])) {
                    $pathFile = $this->pathTemp . "/" . $paths[$request->doc][$request->hash];
                    if (Storage::exists($pathFile)) {
                        Storage::delete($pathFile);
                    }
                    unset($paths[$request->doc]);
                    return response()->json($this->responseHelper->errorCustom(422, "File has been removed."), "hash" => $request->hash]), 422);
                }
                return response()->json($this->responseHelper->errorCustom(82401, "Unable to remove file."), 82401);
            } catch (\Exception $e) {
                save_msg_log($e->getMessage(), "info");
                return response()->json($this->responseHelper->errorCustom(82402, "Unable to remove file."), 82402);
            }
        }
        return response()->json($this->responseHelper->errorCustom(422, "Unknown Error."), 422);
    }
    public function removeOldFile($file)
    {
        $fullpath = $this->pathTemp . "/" . $file;
        if (!empty($file) && Storage::exists($fullpath)) {
            Storage::delete($fullpath);
        }
    }
    private function removeExistFiles($files)
    {
        while (!empty($files)) {
            try {
                foreach ($files as $key => $file) {
                    $this->removeOldFile($file);
                }
            } catch (\Exception $e) {
            }
        }
    }
    private function moveFiles($files)
    {
        if (!empty($files)) {
            foreach ($files as $key => $file) {
                $tempPath = $this->pathTemp . "/" . $file;
                $savePath = $this->pathSave . "/" . $file;
                if (Storage::exists($tempPath)) {
                    if (Storage::exists($savePath)) {
                        Storage::delete($savePath);
                    }
                    Storage::copy($tempPath, $savePath);
                }
            }
        }
    }
    private function moveSubmittedFiles($session)
    {
        if (!blank($session)) {
            $mainDoc = $session->document("main");
            $this->moveFiles(data_get($mainDoc, "files", []));
            $proof = data_get($session, "docs.proof", []);
            if (!empty($proof)) {
                $bsDoc = $session->document("bs");
                if (!blank($bsDoc)) {
                    $bsFiles = data_get($bsDoc, "files", []);
                    if (in_array("bs", $proof)) {
                        $this->moveFiles($bsFiles);
                    } else {
                        $this->removeExistFiles($bsFiles);
                        $bsDoc->files = [];
                        $bsDoc->state = KycDocStatus::INVALID;
                        $bsDoc->save();
                    }
                }
                $ubDoc = $session->document("ub");
                if (!blank($ubDoc)) {
                    $ubFiles = data_get($ubDoc, "files", []);
                    if (in_array("ub", $proof)) {
                        $this->moveFiles($ubFiles);
                    } else {
                        $this->removeExistFiles($ubFiles);
                        $ubDoc->files = [];
                        $ubDoc->state = KycDocStatus::INVALID;
                        $ubDoc->save();
                    }
                }
            }
        }
    }
    private function removeSubmittedFiles($session)
    {
        while (!blank($session)) {
            try {
                $this->wrapInTransaction(function ($session) {
                    $session->docs = [];
                    $session->save();
                    $mainDoc = $session->document("main");
                    $mainFiles = data_get($mainDoc, "files", []);
                    if (!blank($mainDoc)) {
                        $this->removeExistFiles($mainFiles);
                        $mainDoc->files = [];
                        $mainDoc->meta = [];
                        $mainDoc->save();
                    }
                    $bsDoc = $session->document("bs");
                    $bsFiles = data_get($bsDoc, "files", []);
                    if (!blank($bsDoc)) {
                        $this->removeExistFiles($bsFiles);
                        $bsDoc->files = [];
                        $bsDoc->save();
                    }
                    $ubDoc = $session->document("ub");
                    $ubFiles = data_get($ubDoc, "files", []);
                    if (!blank($ubDoc)) {
                        $this->removeExistFiles($ubFiles);
                        $ubDoc->files = [];
                        $ubDoc->save();
                    }
                }, $session);
            } catch (\Exception $e) {
            }
        }
    }
    private function docsAlter($key = NULL)
    {
        $altdoc = data_get(gss("kyc_docs"), "alter");
        $options = ["opt" => false, "req" => false, "bs" => false, "ub" => false, "check" => false];
        if (!empty($altdoc) && is_array($altdoc)) {
            $ub = array_key_exists("ub", $altdoc);
            $bs = array_key_exists("bs", $altdoc);
            $req = array_key_exists("required", $altdoc) && $ub && $bs;
            $options = ["opt" => $bs || $ub ? true : false, "req" => $req, "bs" => $bs, "ub" => $ub];
        }
        if (!empty($key)) {
            return isset($options[$key]) ? $options[$key] : NULL;
        }
        return $options;
    }
    private function docsMain($key = NULL)
    {
        $setting = data_get(gss("kyc_docs"), "main");
        $options = ["opt" => false, "pp" => false, "nid" => false, "dvl" => false, "proof" => false];
        if (!empty($setting) && is_array($setting)) {
            $pp = array_key_exists("pp", $setting);
            $nid = array_key_exists("nid", $setting);
            $dvl = array_key_exists("dvl", $setting);
            $proof = gss("kyc_doc_selfie", "no") == "yes" ? true : false;
            $options = ["opt" => $pp || $nid || $dvl ? true : false, "pp" => $pp, "nid" => $nid, "dvl" => $dvl, "proof" => $proof];
        }
        if (!empty($key)) {
            return isset($options[$key]) ? $options[$key] : NULL;
        }
        return $options;
    }
    private function fileCleanup($session, $clean = NULL)
    {
        if (!blank($session)) {
            if ($clean == "document") {
                $uploaded = "main_files";
            } else {
                if ($clean == "additional") {
                    $uploaded = "proof_files";
                } else {
                    $uploaded = "uploaded_files";
                }
            }
            $mainFiles = Session::get($uploaded, []);
            if (!empty($mainFiles)) {
                foreach ($mainFiles as $key => $val) {
                    $file = array_values($val);
                    if (!empty($file[0])) {
                        if ($clean == "document") {
                            $mainDocs = $session->document("main");
                            $oldFiles = data_get($mainDocs, "files", []);
                            if (blank($mainDocs) || empty($oldFiles)) {
                                $this->removeOldFile($file[0]);
                            }
                            if (!empty($oldFiles[$key]) && $oldFiles[$key] != $file[0]) {
                                $this->removeOldFile($file[0]);
                            }
                        } else {
                            if ($clean == "additional") {
                                $bsDocs = $session->document("bs");
                                $bsFiles = data_get($bsDocs, "files", []);
                                if (blank($bsDocs) || empty($bsFiles)) {
                                    $this->removeOldFile($file[0]);
                                }
                                if (!empty($bsFiles[$key]) && $bsFiles[$key] != $file[0]) {
                                    $this->removeOldFile($file[0]);
                                }
                                $ubDocs = $session->document("ub");
                                $ubFiles = data_get($ubDocs, "files", []);
                                if (blank($ubDocs) || empty($ubFiles)) {
                                    $this->removeOldFile($file[0]);
                                }
                                if (!empty($ubFiles[$key]) && $ubFiles[$key] != $file[0]) {
                                    $this->removeOldFile($file[0]);
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    private function isInvalidFile($file)
    {
        if (!empty($file)) {
            $extension = explode(".", $file);
            if (str_contains($file, "/") || empty($extension[1])) {
                return true;
            }
            if (!in_array($extension[1], ["jpg", "jpeg", "png"])) {
                return true;
            }
            return false;
        }
        return false;
    }
    private function hasDocsFiles($document)
    {
        if (blank($document) || !data_get($document, "files", [])) {
            return false;
        }
        return true;
    }
    private function addNewApplicant()
    {
        $userId = auth()->user()->id;
        $reference = $this->kycModule->generateReference();
        $kycApplicant = new KycApplicants();
        $kycApplicant->user_id = $userId;
        $kycApplicant->reference = $reference;
        $kycApplicant->status = KycStatus::STARTED;
        $kycApplicant->save();
        return $kycApplicant->fresh();
    }
    private function addNewSession($profile)
    {
        $userId = auth()->user()->id;
        $sessionId = $this->kycModule->generateUniqueSession();
        $newSession = new KycSessions();
        $newSession->user_id = $userId;
        $newSession->session = $sessionId;
        $newSession->profile = $profile;
        $newSession->method = "manual";
        $newSession->status = KycSessionStatus::STARTED;
        $newSession->save();
        return $newSession->fresh();
    }
}

?>