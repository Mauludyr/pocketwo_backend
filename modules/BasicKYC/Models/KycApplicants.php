<?php

namespace Modules\BasicKYC\Models;

use App\Models\User;
use Illuminate\Support\Arr;
use Illuminate\Database\Eloquent\Model;
use Modules\BasicKYC\Models\KycSessions;
use Modules\BasicKYC\Models\KycDocs;

class KycApplicants extends Model
{
    use \App\Filters\Filterable;
    protected $casts = [];
    public function user()
    {
        return $this->belongsTo(User::class, "user_id");
    }
    public function sessions()
    {
        return $this->hasMany(KycSessions::class, "user_id", "user_id");
    }
    public function entry()
    {
        return $this->sessions->sortByDesc("id")->first();
    }
    public function profile($key = NULL)
    {
        $entry = $this->entry();
        $metas = data_get($entry, "profile", []);
        if (!empty($key)) {
            return Arr::get($metas, $key) ? Arr::get($metas, $key) : false;
        }
        return $metas;
    }
    public function docs()
    {
        return $this->hasMany(KycDocs::class, "user_id", "user_id");
    }
    public function getUserReferenceAttribute()
    {
        $key = config("modules.basic-kyc.prefix");
        $prefix = $key ? $key : "";
        return $prefix . $this->attributes["reference"];
    }
}

?>