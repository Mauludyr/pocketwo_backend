<?php

namespace Modules\BasicKYC\Models;

use App\Models\User;
use Modules\BasicKYC\Models\KycSessions;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
class KycDocs extends Model
{
    use HasFactory;
    protected $fillable = [];
    protected $casts = ["files" => "array", "meta" => "array"];
    public function user()
    {
        return $this->belongsTo(User::class, "user_id");
    }
    public function session()
    {
        return $this->belongsTo(KycSessions::class, "session_id");
    }
}

?>