<?php

namespace Modules\BasicKYC;

use App\Services\SettingsService;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Http;
use Modules\BasicKYC\Models\KycApplicants;
use Modules\BasicKYC\Models\KycSessions;
use Modules\BasicKYC\Helpers\KycStatus;

class BasicKYCModule
{
    private $bySlug = NULL;
    const VERSION = "1.0.1";
    const LAST_UPDATE = "01142022";
    const MIN_APP_VER = "1.2.0";
    const MOD_TYPES = "module";
    const SLUG = "basic-kyc";
    const METHOD = "manual";
    public function __construct()
    {
        $this->bySlug = "basic_kyc";
    }
    public function minAppVer()
    {
        return self::MIN_APP_VER;
    }
    public function getVer()
    {
        return self::VERSION;
    }
    public function getSlug()
    {
        return self::SLUG;
    }
    public function compatible()
    {
        return 0 <= version_compare(config("app.version"), $this->minAppVer()) ? true : false;
    }
    public function messages($key, $type)
    {
        $messages = base_path(implode(DIRECTORY_SEPARATOR, ["modules", "BasicKYC", "Config", "messages.php"]));
        if (!file_exists($messages)) {
            return false;
        }
        $messages = collect(include $messages);
        return $messages->get($type)[$key];
    }
    public function alert($attr = [])
    {
        if (feature_enable("kyc") && gss("kyc_verification") == "yes" && !auth()->user()->kyc_verified && !auth()->user()->kyc_rejected && auth()->user()->is_verified) {
            $getKycApplicant = KycApplicants::where("user_id", auth()->user()->id)->first();
            if (data_get($getKycApplicant, "status") == KycStatus::RESUBMIT) {
                $message = __("Please resubmit your all the valid documents to verify your identity.");
                $attr["type"] = "info";
            } else {
                if (!data_get($getKycApplicant, "status") || data_get($getKycApplicant, "status") == KycStatus::STARTED) {
                    $attr["type"] = "primary";
                    $message = __("To be compliant and to protect your account, please verify your identity by submitting document.");
                }
            }
            return view("BasicKYC::user.misc.alert", compact("message", "attr", "getKycApplicant"))->render();
        }
        return false;
    }
    public static function getFields($type = NULL)
    {
        $allFields = [
            "profile" => [
                "first_name" => [
                    "label" => __("First Name"), 
                    "default" => [
                        "show" => true, 
                        "required" => true, 
                        "disabled" => true
                    ]
                ],
                "last_name" => [
                    "label" => __("Last Name"), 
                    "default" => [
                        "show" => true, 
                        "required" => true, 
                        "disabled" => true
                    ]
                ], 
                "dob" => [
                    "label" => __("Date of Birth"), 
                    "default" => [
                        "show" => true, 
                        "required" => true
                    ]
                ], 
                "phone" => [
                    "label" => __("Phone Number"), 
                    "default" => [
                        "show" => false, 
                        "required" => false
                    ]
                ], 
                "gender" => [
                    "label" => __("Gender"), 
                    "default" => [
                        "show" => false, 
                        "required" => false
                    ]
                ], 
                "nationality" => [
                    "label" => __("Nationality"), 
                    "default" => [
                        "show" => true, 
                        "required" => false
                    ]
                ],
                "country_of_residence" => [
                    "label" => __("Country Of Residence"), 
                    "default" => [
                        "show" => true, 
                        "required" => true, 
                        "disabled" => true
                    ]
                ], 
                "year_in_country" => [
                    "label" => __("Years in Country of Residence"), 
                    "default" => [
                        "show" => true, 
                        "required" => true
                    ]
                ],
                "address" => [
                    "label" => __("Address"), 
                    "default" => [
                        "show" => true, 
                        "required" => false
                    ]
                ],
                "education" => [
                    "label" => __("Education"), 
                    "default" => [
                        "show" => true, 
                        "required" => false
                    ]
                ],
                "marital_status" => [
                    "label" => __("Marital Status"), 
                    "default" => [
                        "show" => true, 
                        "required" => false
                    ]
                ],
                "children" => [
                    "label" => __("Children"), 
                    "default" => [
                        "show" => true, 
                        "required" => false
                    ]
                ],
                "income_type" => [
                    "label" => __("Income Type"), 
                    "default" => [
                        "show" => true, 
                        "required" => false
                    ]
                ],
                "monthly_income" => [
                    "label" => __("Monthly Income"), 
                    "default" => [
                        "show" => true, 
                        "required" => false
                    ]
                ],
                "monthly_liabilities" => [
                    "label" => __("Monthly Liabilities"), 
                    "default" => [
                        "show" => true, 
                        "required" => false
                    ]
                ],
                "latest_utility_bill" => [
                    "label" => __("Latest Utility Bill"), 
                    "default" => [
                        "show" => true, 
                        "required" => false
                    ]
                ],
                "current_savings_balance" => [
                    "label" => __("Current Savings Balance"), 
                    "default" => [
                        "show" => true, 
                        "required" => false
                    ]
                ],
                "other_assets" => [
                    "label" => __("Other Assets"), 
                    "default" => [
                        "show" => true, 
                        "required" => false
                    ]
                ],
                "credit_bureau_queries" => [
                    "label" => __("Credit Bureau Queries"), 
                    "default" => [
                        "show" => true, 
                        "required" => false
                    ]
                ],
            ], 
            "docs" => [
                "main" => [
                    "pp" => [
                        "label" => __("Passport"), 
                        "default" => [
                            "show" => true
                        ], 
                        "note" => __("Allow passport for user identity verification."), 
                        "files" => [
                            "main"
                        ]
                    ], 
                    "nid" => [
                        "label" => __("National ID"), 
                        "default" => [
                            "show" => true
                        ], 
                        "note" => __("Allow national ID for user identity verification."), 
                        "files" => [
                            "main", 
                            "back"
                        ]
                    ], 
                    "dvl" => [
                        "label" => __("Driving License"), 
                        "default" => [
                            "show" => false
                        ], 
                        "note" => __("Allow driving license for user identity verification."), 
                        "files" => [
                            "main", "back"
                        ]
                    ]
                ], 
                "additional" => [
                    "bs" => [
                        "label" => __("Bank Statement"), 
                        "default" => [
                            "show" => true, 
                            "required" => true
                        ], 
                        "note" => __("Allow bank statements for additional verification."), 
                        "files" => [
                            "main"
                        ]
                    ], 
                    "ub" => [
                        "label" => __("Utility Bill"), 
                        "default" => [
                            "show" => false, 
                            "required" => false
                        ], 
                        "note" => __("Allow utility bill for additional verification."), 
                        "files" => [
                            "main"
                        ]
                    ]
                ]
            ],
            "question" =>[
                "question_1" => [
                    "label" => __("Full Name"), 
                    "default" => [
                        "show" => true, 
                        "required" => true
                    ]
                ], 
                "question_1" => [
                    "label" => __("Date of Birth"), 
                    "default" => [
                        "show" => true, 
                        "required" => true
                    ]
                ], 
            ],
            "social_media_links" =>[
                "facebook" => [
                    "label" => __("Facebook"), 
                    "default" => [
                        "show" => true, 
                        "required" => true
                    ]
                ], 
                "instagram" => [
                    "label" => __("Instagram"), 
                    "default" => [
                        "show" => true, 
                        "required" => true
                    ]
                ], 
                "twitter" => [
                    "label" => __("Twitter"), 
                    "default" => [
                        "show" => true, 
                        "required" => true
                    ]
                ], 
                "telegram" => [
                    "label" => __("Telegram"), 
                    "default" => [
                        "show" => true, 
                        "required" => true
                    ]
                ] 
            ]
        ];
        if (!empty($type)) {
            return isset($allFields[$type]) ? $allFields[$type] : [];
        }
        return $allFields;
    }
    public function generateReference()
    {
        $latest = KycApplicants::orderBy("id", "desc")->latest()->first();
        $nextid = isset($latest->id) ? sprintf("%03s", $latest->id + 1) : sprintf("%03s", 1);
        $reference = (int) (mt_rand(10, 99) . substr(time(), -3) . $nextid);
        $existing = KycApplicants::where("reference", $reference)->first();
        if (blank($existing)) {
            return $reference;
        }
        return $this->generateReference();
    }
    public function generateUniqueSession()
    {
        $userId = auth()->user()->id;
        $count = KycSessions::where("user_id", $userId)->count();
        $nextid = $count ? sprintf("%04s", $count + 1) : sprintf("%04s", 1);
        $session = cipher($userId) . "-" . substr(time(), -8) . "-" . $nextid;
        $existing = KycSessions::where("session", $session)->first();
        if (blank($existing)) {
            return $session;
        }
        return $this->generateUniqueSession();
    }
}

?>