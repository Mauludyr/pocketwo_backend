<?php

namespace Modules\WdCrypto\Provider;

use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Route;

class RouteServiceProvider extends ServiceProvider
{
    protected $ModuleNameSpace = "\\Modules\\WdCrypto\\Controllers";
    public function boot()
    {
        parent::boot();
    }
    public function map()
    {
        $this->mapModuleRoutes();
    }
    protected function mapModuleRoutes()
    {
        Route::middleware(["web", "auth"])->namespace($this->ModuleNameSpace)->group(__DIR__ . "/../Routes/route.php");
    }
}

?>