<?php

use Illuminate\Support\Facades\Route;

Route::name('admin.settings.gateway.payment.')->middleware(['auth', 'admin'])->prefix('admin/settings/gateway/payment-method')->group(function () {
    Route::get('/xanpool', 'WalletSettingsController@settingsView')->name('xanpool');
    Route::post('/xanpool', 'WalletSettingsController@saveWalletSettings')->name('xanpool.save');
});

Route::middleware(['user'])->group(function(){
    Route::get('xanpool/deposit-complete', 'TransactionConfirmationController@depositComplete')->name('user.xanpool.deposit.complete');
    Route::post('xanpool/deposit/reference', 'TransactionConfirmationController@saveReference')->name('user.xanpool.deposit.reference');
});

