<?php

use Modules\Xanpool\XanpoolModule;

return [
    XanpoolModule::SLUG => [
        'name' => __('Xanpool'),
        'slug' => XanpoolModule::SLUG,
        'method' => XanpoolModule::METHOD,
        'icon' => 'ni-wallet-fill',
        'full_icon' => 'ni-wallet-fill',
        'is_online' => false,
        'processor_type' => 'payment',
        'processor' => XanpoolModule::class,
        'rounded' => 0,
        'supported_currency' => [
            'BTC', 'ETH', 'LTC', 'BCH', 'BNB', 'ADA', 'XRP', 'USDC', 'USDT', 'TRX'
        ],
        'system' => [
            'kind' => 'Payment',
            'info' => 'Manual / Offline',
            'type' => XanpoolModule::MOD_TYPES,
            'version' => XanpoolModule::VERSION,
            'update' => XanpoolModule::LAST_UPDATE,
            'description' => 'Accept crypto related payments manually from user.',
            'addons' => false,
        ]
    ],
];
