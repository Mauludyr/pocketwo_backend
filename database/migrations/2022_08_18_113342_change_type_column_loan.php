<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeTypeColumnLoan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('loan_requests', function (Blueprint $table) {
            $table->float('loan_amount', 10, 2)->default(0)->change();
            $table->float('min_loan_repayment', 10, 2)->default(0)->change();
            $table->float('max_loan_repayment', 10, 2)->default(0)->change();
            $table->float('min_loan_interest', 10, 2)->default(0)->change();
            $table->float('max_loan_interest', 10, 2)->default(0)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('loan_requests', function (Blueprint $table) {
            
        });
    }
}
