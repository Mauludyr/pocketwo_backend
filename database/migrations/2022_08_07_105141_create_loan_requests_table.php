<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLoanRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('loan_requests', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->integer('loan_amount')->nullable();
            $table->integer('loan_duration')->nullable();
            $table->integer('min_interest_rate')->nullable();
            $table->integer('max_interest_rate')->nullable();
            $table->string('interest_rate_type')->nullable();
            $table->integer('min_loan_repayment')->nullable();
            $table->integer('max_loan_repayment')->nullable();
            $table->integer('min_loan_interest')->nullable();
            $table->integer('max_loan_interest')->nullable();
            $table->string('wallet_address')->nullable();
            $table->longText("profile")->nullable();
            $table->longText("documents")->nullable();
            $table->string('status')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('loan_requests');
    }
}
