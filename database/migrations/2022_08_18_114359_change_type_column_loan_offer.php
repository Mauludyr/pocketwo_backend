<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeTypeColumnLoanOffer extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('loan_offers', function (Blueprint $table) {
            $table->float('late_charges', 10, 2)->default(0)->change();
            $table->float('interest_rate', 10, 2)->default(0)->change();
            $table->float('interest_amount', 10, 2)->default(0)->change();
            $table->float('repayment_amount', 10, 2)->default(0)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('loan_offers', function (Blueprint $table) {
            //
        });
    }
}
