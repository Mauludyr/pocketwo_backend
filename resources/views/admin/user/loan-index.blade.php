@php
    use Carbon\Carbon;
@endphp

@extends('admin.layouts.master')
@section('title', __('Verification Center'))

@section('content')
<div class="nk-content-body">
    <div class="nk-block-head nk-block-head-sm">
        <div class="nk-block-between-md g-3">
            <div class="nk-block-head-content">
                <h3 class="nk-block-title page-title">
                    {{ __("Detail Loan") }} / 
                    <span class="text-grey fw-normal">{{ $data->id }}</span>
                </h3>
                <div class="nk-block-des text-soft">
                    <ul class="list-inline">
                        <li>
                            <span class="badge badge-xs {{ $data->status }}">
                            {{ $data->status }}
                            </span>
                        </li>
                        <li>{{ __('User:') }} <span class="text-base">{{ $data->user->name . ' / ' .the_uid($data->user_id) }}</span></li>
                    </ul>
                </div>
            </div>
            <div class="nk-block-head-content">
                <ul class="nk-block-tools gx-1">
                    <li class="order-md-last {{ $type == 'request' ? '' : 'd-none' }}">
                        <a href="javascript:void(0)" class="btn btn-primary" data-toggle="modal" data-target="#reject-entry{{ $data->id }}"><em class="icon ni ni-check"></em> <span>{{ __("Make Offer") }}</span></a>
                    </li>
                    @if ($type == 'request')
                        <li class="order-md-last">
                            <a href="javascript:void(0)" class="btn btn-secondary" data-toggle="modal" data-target="#active-entry{{ $data->id }}"><em class="icon ni ni-info-fill"></em> <span>{{ $data->status == 'Active' ? __("Change to Deactive") : __("Change to Active") }}</span></a>
                        </li>
                        @if($data->status == 'Pending Test Amount')
                            <li class="order-md-last">
                                <a href="javascript:void(0)" class="btn btn-danger" data-toggle="modal" data-target="#hash-entry{{ $data->id }}"><em class="icon ni ni-info-fill"></em> <span>{{ __("Input Hash") }}</span></a>
                            </li>
                        @else
                            <li class="order-md-last">
                                <a href="javascript:void(0)" class="btn btn-danger" data-toggle="modal" data-target="#test-entry{{ $data->id }}"><em class="icon ni ni-info-fill"></em> <span>{{ __("Change to Pending Test Amount") }}</span></a>
                            </li>
                        @endif
                        <li class="order-md-last">
                            <a href="javascript:void(0)" class="btn btn-info" data-toggle="modal" data-target="#repaid-entry{{ $data->id }}"><em class="icon ni ni-info-fill"></em> <span>{{ __("Change to Repaid") }}</span></a>
                        </li>
                    @endif
                    <li class="order-md-first ml-auto">
                        <a href="{{ $type == 'request' ? route('admin.loans.request') : route('admin.loans.offer') }}" class="btn btn-outline-light bg-white d-none d-sm-inline-flex">
                            <em class="icon ni ni-arrow-left"></em>
                            <span>{{ __('Back') }}</span>
                        </a>
                        <a href="#" class="btn btn-icon btn-outline-light bg-white d-inline-flex d-sm-none">
                            <em class="icon ni ni-arrow-left"></em>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <div class="nk-block">
        <div class="row gy-gs">
            <div class="col-xxl-5">
                <div class="card card-bordered card-stretch card-full">
                    <ul class="data-list is-compact">
                        <li class="data-item">
                            <h6 class="card-title">{{ __("Loan Info") }}</h6>
                        </li>
                        <li class="data-item d-none">
                            <div class="data-col">
                                <div class="data-label">{{ __("Session ID") }}</div>
                                <div class="data-value">{{ $data->session }}</div>
                            </div>
                        </li>
                        <li class="data-item d-none">
                            <div class="data-col">
                                <div class="data-label">{{ __("Submitted By") }}</div>
                                <div class="data-value">{{ the_uid($data->user_id) }}</div>
                            </div>
                        </li>
                        <li class="data-item d-none">
                            <div class="data-col">
                                <div class="data-label">{{ __("Submitted At") }}</div>
                                <div class="data-value">{{ show_date($data->created_at, true) }}</div>
                            </div>
                        </li>
                        <li class="data-item d-none">
                            <div class="data-col">
                                <div class="data-label">{{ __("Checked At") }}</div>
                                <div class="data-value">
                                    {{ data_get($data, 'checked_at') ? show_date(data_get($data, 'checked_at'), true) : __("Not checked yet") }}
                                </div>
                            </div>
                        </li>
                        <li class="data-item d-none">
                            <div class="data-col">
                                <div class="data-label">{{ __("Checked By") }}</div>
                                <div class="data-value">
                                    @if (data_get($data, 'checked_by'))
                                        {{ data_get($data, 'checked_by.name') }} <span class="text-soft small">({{ is_admin() ? __('Admin') : '' }})</span>
                                    @else
                                        {{ __("Not checked yet") }}
                                    @endif
                                </div>
                            </div>
                        </li>
                        <li class="data-item d-none">
                            <div class="data-col">
                                <span class="lead-text fw-medium">{{ __("Submission Information") }}</span>
                            </div>
                            <div class="data-value justify-end">
                                <a class="link link-sm" href="{{ route('admin.users.details', ['id' => $data->user->id, 'type' => 'personal']) }}" target="_blank">{{ __("View Profile") }}</a>
                            </div>
                        </li>
                        <li class="data-item">
                            <div class="data-col">
                                <div class="data-label">{{ __("Name") }}</div>
                                <div class="data-value">
                                    {{ data_get($data, 'profile.first_name') ? data_get($data, 'profile.first_name') : __('Not given by user') }} {{ data_get($data, 'profile.last_name') ? data_get($data, 'profile.last_name') : __('Not given by user') }}
                                </div>
                            </div>
                        </li>
                        <li class="data-item d-none">
                            <div class="data-col">
                                <div class="data-label">{{ __("Last Name") }}</div>
                                <div class="data-value">
                                    {{ data_get($data, 'profile.last_name') ? data_get($data, 'profile.last_name') : __('Not given by user') }}
                                </div>
                            </div>
                        </li>
                        <li class="data-item">
                            <div class="data-col">
                                <div class="data-label">{{ __("Gender") }}</div>
                                <div class="data-value">
                                    0
                                </div>
                            </div>
                        </li>
                        <li class="data-item">
                            <div class="data-col">
                                <div class="data-label">{{ __("Date of Birth") }}</div>
                                <div class="data-value">
                                    0
                                </div>
                            </div>
                        </li>
                        <li class="data-item">
                            <div class="data-col">
                                <div class="data-label">{{ __("Mobile Number") }}</div>
                                <div class="data-value">
                                    0
                                </div>
                            </div>
                        </li>
                        <li class="data-item">
                            <div class="data-col">
                                <div class="data-label">{{ __("Nationality") }}</div>
                                <div class="data-value">
                                    0
                                </div>
                            </div>
                        </li>
                        <li class="data-item">
                            <div class="data-col">
                                <div class="data-label">{{ __("Country of Residence / Year") }}</div>
                                <div class="data-value">
                                    0
                                </div>
                            </div>
                        </li>
                        <li class="data-item d-none">
                            <div class="data-col">
                                <div class="data-label">{{ __("Year in Country") }}</div>
                                <div class="data-value">
                                    0
                                </div>
                            </div>
                        </li>
                        <li class="data-item">
                            <div class="data-col">
                                <div class="data-label">{{ __("Address") }}</div>
                                <div class="data-value">
                                    0
                                </div>
                            </div>
                        </li>
                        <li class="data-item">
                            <div class="data-col">
                                <div class="data-label">
                                    {{ __("Social Media") }} <br>
                                    {{ __("Twitter") }} /
                                    {{ __("Facebook") }} /
                                    {{ __("Instagram") }} /
                                    {{ __("Tiktok") }}
                                </div>
                                <div class="data-value">
                                    0
                                </div>
                            </div>
                        </li>
                        <li class="data-item d-none">
                            <div class="data-col">
                                <div class="data-label">{{ __("Facebook") }}</div>
                                <div class="data-value">
                                    0
                                </div>
                            </div>
                        </li>
                        <li class="data-item d-none">
                            <div class="data-col">
                                <div class="data-label">{{ __("Instagram") }}</div>
                                <div class="data-value">
                                    0
                                </div>
                            </div>
                        </li>
                        <li class="data-item d-none">
                            <div class="data-col">
                                <div class="data-label">{{ __("Tiktok") }}</div>
                                <div class="data-value">
                                    0
                                </div>
                            </div>
                        </li>
                        <li class="data-item">
                            <div class="data-col">
                                <div class="data-label">{{ __("Education") }}</div>
                                <div class="data-value">
                                    0
                                </div>
                            </div>
                        </li>
                        <li class="data-item">
                            <div class="data-col">
                                <div class="data-label">{{ __("Marital Status") }}</div>
                                <div class="data-value">
                                    0
                                </div>
                            </div>
                        </li>
                        <li class="data-item">
                            <div class="data-col">
                                <div class="data-label">{{ __("Children") }}</div>
                                <div class="data-value">
                                    0
                                </div>
                            </div>
                        </li>
                        <li class="data-item">
                            <div class="data-col">
                                <div class="data-label">{{ __("Monthly Income (Source)") }}</div>
                                <div class="data-value">
                                    0
                                </div>
                            </div>
                        </li>
                        <li class="data-item d-none">
                            <div class="data-col">
                                <div class="data-label">{{ __("Source Income") }}</div>
                                <div class="data-value">
                                    0
                                </div>
                            </div>
                        </li>
                        <li class="data-item d-none">
                            <div class="data-col">
                                <div class="data-label">{{ __("Monthly Liabilities") }}</div>
                                <div class="data-value">
                                    0
                                </div>
                            </div>
                        </li>
                        <li class="data-item">
                            <div class="data-col">
                                <div class="data-label">{{ __("KYC Score") }}</div>
                                <div class="data-value">
                                    0
                                </div>
                            </div>
                        </li>
                        <li class="data-item">
                            <div class="data-col">
                                <div class="data-label">{{ __("Loan Limit") }}</div>
                                <div class="data-value">
                                    0
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-xxl-7">
                <div class="card card-bordered card-stretch card-full">
                    <ul class="data-list is-compact">
                        <li class="data-item">
                            <h6 class="card-title">{{ __("Documents for Loans") }}</h6>
                        </li>
                        <li class="data-item d-none">
                            <div class="data-col">
                                <div class="data-label">{{ __("Main Document") }}</div>
                                <div class="data-value">{{ short_to_docs($data->main_doc_type) }}</div>
                            </div>
                        </li>
                        @if (!empty($data->main_doc_meta))
                            @foreach($data->main_doc_meta as $meta => $value)
                                @if(!empty($value))
                                <li class="data-item d-none">
                                    <div class="data-col">
                                        <div class="data-label">
                                            @if ($meta == 'country')
                                                {{ __("Issued by Country") }}
                                            @elseif ($meta == 'number')
                                                {{ __("ID Number") }}
                                            @elseif ($meta == 'issue')
                                                {{ __("Issue Date") }}
                                            @elseif ($meta == 'expiry')
                                                {{ __("Expiry Date") }}
                                            @else
                                                {{ __(ucfirst($meta)) }}
                                            @endif
                                        </div>
                                        <div class="data-value">{{ $value }}</div>
                                    </div>
                                </li>
                                @endif
                            @endforeach
                        @endif
                    </ul>
                    <div class="divider md mt-0"></div>
                    <div class="card-inner pt-0">
                        <div class="title mb-2 d-none">
                            <span class="lead-text fw-medium">{{ __("Uploaded Document") }}</span>
                        </div>
                        <div class="py-1 font-italic">
                            {{ __("No uploaded document found!") }}
                        </div>
                    </div>
                    @if($type == 'offer')
                        <div class="divider md mt-0"></div>
                        <ul class="data-list is-compact">
                            <li class="data-item">
                                <h6 class="card-title">{{ __("Loans Aggrement") }}</h6>
                            </li>
                        </ul>
                        <div class="card-inner pt-0">
                            <div class="title mb-2">
                                <span class="lead-text fw-medium">{!! $data->loan_agreement !!}</span>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('modal')
    <div class="modal fade" role="dialog" id="active-entry{{ $data->id }}">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <a href="javascript:void(0)" class="close" data-dismiss="modal"><em class="icon ni ni-cross-sm"></em></a>
                <div class="modal-body">
                    <div class="row gy-3">
                        <div class="col-md-12">
                            <h5 class="title mb-2">{{ $data->status == 'Active' ? __("Change to Deactive") : __("Change to Active") }}</h5>
                        </div>
                    </div>
                    <ul class="align-center flex-nowrap gx-2 pt-2 mt-2">
                        <li>
                            @if ($data->status == 'Active')
                                <a href="#" class="btn btn-primary loan-request-update" data-action="deactive"><span>{{ __("Confirm") }}</span></a>
                            @else
                                <a href="#" class="btn btn-primary loan-request-update" data-action="active"><span>{{ __("Confirm") }}</span></a>
                            @endif
                        </li>
                        <li>
                            <button data-dismiss="modal" type="button" class="btn btn-danger">{{ __('Cancel') }}</button>
                        </li>
                    </ul>

                    <div class="divider md stretched"></div>
                    <div class="notes">
                        <ul>
                            <li class="alert-note is-plain text-danger">
                                <em class="icon ni ni-alert"></em>
                                <p>{{ __("You can not undo this action once you take action.") }}</p>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" role="dialog" id="repaid-entry{{ $data->id }}">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <a href="javascript:void(0)" class="close" data-dismiss="modal"><em class="icon ni ni-cross-sm"></em></a>
                <div class="modal-body">
                    <div class="row gy-3">
                        <div class="col-md-12">
                            <h5 class="title mb-2">{{ __('Change to Repaid') }}</h5>
                        </div>
                    </div>
                    <ul class="align-center flex-nowrap gx-2 pt-2 mt-2">
                        <li>
                            <a href="#" class="btn btn-primary loan-request-update" data-action="repaid"><span>{{ __("Confirm") }}</span></a>
                        </li>
                        <li>
                            <button data-dismiss="modal" type="button" class="btn btn-danger">{{ __('Cancel') }}</button>
                        </li>
                    </ul>

                    <div class="divider md stretched"></div>
                    <div class="notes">
                        <ul>
                            <li class="alert-note is-plain text-danger">
                                <em class="icon ni ni-alert"></em>
                                <p>{{ __("You can not undo this action once you take action.") }}</p>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" role="dialog" id="test-entry{{ $data->id }}">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <a href="javascript:void(0)" class="close" data-dismiss="modal"><em class="icon ni ni-cross-sm"></em></a>
                <div class="modal-body">
                    <div class="row gy-3">
                        <div class="col-md-12">
                            <h5 class="title mb-2">{{ __('Change to Pending Test Amount') }}</h5>
                        </div>
                    </div>
                    <ul class="align-center flex-nowrap gx-2 pt-2 mt-2">
                        <li>
                            <a href="#" class="btn btn-primary loan-request-update" data-action="pending-test-amount"><span>{{ __("Confirm") }}</span></a>
                        </li>
                        <li>
                            <button data-dismiss="modal" type="button" class="btn btn-danger">{{ __('Cancel') }}</button>
                        </li>
                    </ul>

                    <div class="divider md stretched"></div>
                    <div class="notes">
                        <ul>
                            <li class="alert-note is-plain text-danger">
                                <em class="icon ni ni-alert"></em>
                                <p>{{ __("You can not undo this action once you take action.") }}</p>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" role="dialog" id="reject-entry{{ $data->id }}">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <a href="javascript:void(0)" class="close" data-dismiss="modal"><em class="icon ni ni-cross-sm"></em></a>
                <div class="modal-body">
                    <div class="row gy-3">
                        <div class="col-md-12">
                            <h5 class="title mb-2">{{ __('Make Offer') }}</h5>
                        </div>
                    </div>
                    <div class="row gy-4">
                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="form-label-group">
                                    <label class="form-label" for="provider-name">{{ __('Provider Name') }}  <span class="text-danger">*</span></label>
                                </div>

                                <div class="form-control-wrap">
                                    <input type="text" name="name" value="{{ auth()->user()->name }}" class="form-control form-control-lg" id="provider_name" placeholder="{{ __('Enter full name') }}" required maxlength="190">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="form-label-group">
                                    <label class="form-label" for="interest_amount">{{ __('Interest Amount') }} <span class="text-danger">*</span></label>
                                </div>
                                <div class="form-control-wrap">
                                    <input type="number" name="interest_amount" class="form-control form-control-lg" id="interest_amount" placeholder="{{ __('Enter Interest Amount') }}" required maxlength="190">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="form-label-group">
                                    <label class="form-label" for="interest_rate">{{ __('Interest Rate') }} <span class="text-danger">*</span></label>
                                </div>

                                <div class="form-control-wrap">
                                    <input type="number" name="interest_rate" class="form-control form-control-lg" required id="interest_rate" placeholder="{{ __('Interest Rate') }}">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="form-label-group">
                                    <label class="form-label" for="repayment_amount">{{ __('Repayment Amount') }} <span class="text-danger">*</span></label>
                                </div>

                                <div class="form-control-wrap">
                                    <input type="number" name="repayment_amount" class="form-control form-control-lg" required id="repayment_amount" placeholder="{{ __('Repayment Amount') }}">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="form-label-group">
                                    <label class="form-label" for="late_charges">{{ __('Late Charges') }} <span class="text-danger">*</span></label>
                                </div>

                                <div class="form-control-wrap">
                                    <input type="number" name="late_charges" class="form-control form-control-lg" required id="late_charges" placeholder="{{ __('Late Charges') }}">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="divider md stretched"></div>
                    <!-- <div class="notes"> -->
                        <ul class="align-center flex-nowrap gx-2 pt-2 mt-2">
                            <li>
                                <a href="#" class="btn btn-primary submit-offer" data-action="loan_offer"><span>{{ __("Submit") }}</span></a>
                            </li>
                            <li>
                                <a href="javascript:void(0)" class="btn btn-danger" data-dismiss="modal"><span>{{ __("Cancel") }}</span></a>
                            </li>
                        </ul>
                    <!-- </div> -->
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" role="dialog" id="hash-entry{{ $data->id }}">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <a href="javascript:void(0)" class="close" data-dismiss="modal"><em class="icon ni ni-cross-sm"></em></a>
                <div class="modal-body">
                    <div class="row gy-3">
                        <div class="col-md-12">
                            <h5 class="title mb-2">{{ __('Transaction Hash') }}</h5>
                        </div>
                    </div>
                    <div class="row gy-4">
                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="form-label-group">
                                    <label class="form-label" for="transaction-hash">{{ __('Transaction Hash') }}  <span class="text-danger">*</span></label>
                                </div>

                                <div class="form-control-wrap">
                                    <input type="text" name="transaction_hash" class="form-control form-control-lg" id="transaction_hash" placeholder="{{ __('Enter Transaction Hash') }}" required maxlength="190">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="divider md stretched"></div>
                    <!-- <div class="notes"> -->
                        <ul class="align-center flex-nowrap gx-2 pt-2 mt-2">
                            <li>
                                <a href="#" class="btn btn-primary submit-hash" data-action="loan_offer"><span>{{ __("Submit") }}</span></a>
                            </li>
                            <li>
                                <a href="javascript:void(0)" class="btn btn-danger" data-dismiss="modal"><span>{{ __("Cancel") }}</span></a>
                            </li>
                        </ul>
                    <!-- </div> -->
                </div>
            </div>
        </div>
    </div>
@endpush

@push('scripts')
<script>
    $('.submit-offer').on('click', function (e) {
        e.preventDefault();
        let $self = $(this), 
            action = {
                    provider_name : $('#provider_name').val(),
                    interest_amount : $('#interest_amount').val(),
                    interest_rate : $('#interest_rate').val(),
                    repayment_amount : $('#repayment_amount').val(),
                    late_charges : $('#late_charges').val()
            }, 
            url = "{{ route('admin.loans.offer.store', $data->id) }}";
		if (url !== null && action) { CustomApp.Form.toPost(url, {action: action}) }
    });
    $('.submit-hash').on('click', function (e) {
        e.preventDefault();
        let $self = $(this), 
            action = {transaction_hash : $('#transaction_hash').val()}, 
            url = "{{ route('admin.loans.hash.store', $data->id) }}";
        if (url !== null && action) { CustomApp.Form.toPost(url, {action: action}) }
    });
    $('.loan-request-update').on('click', function (e) {
        e.preventDefault();
        let $self = $(this), action = $self.data("action"), url = "{{ route('admin.loans.update.status', $data->id) }}";
        if (url !== null && action) { CustomApp.Form.toPost(url, {action: action}) }
    });
</script>
@endpush
