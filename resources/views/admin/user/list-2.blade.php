@extends('admin.layouts.master')

@php 

$byTypes = (!request('state')) ? false : ucfirst(request('state'));

@endphp

@section('title', __($pageTitle))

@section('content')
    <div class="nk-content-body">
        <div class="nk-block-head nk-block-head-sm">
            <div class="nk-block-between">
                <div class="nk-block-head-content">
                    <h3 class="nk-block-title page-title">{{ $pageTitle }} List</h3>
                    <div class="nk-block-des text-soft">
                        <p>{!! __('Total :number :type account.', ['number' => '<strong class="text-base">'.$users->total() .'</strong>', 'type' => '<span class="text-base">'. $pageTitle .'</span>']) !!}</p>
                    </div>
                </div>
            </div>
        </div>
        {{-- nk-block-head --}}
        <div class="nk-block">
            @if(filled($users))
            <div class="card card-bordered card-stretch">
                <div class="card-inner-group">
                    <div class="card-inner position-relative card-tools-toggle"></div>
                    <div class="card-inner p-0">
                        <div class="nk-tb-list nk-tb-ulist{{ user_meta('user_display') == 'compact' ? ' is-compact': '' }}">
                            <div class="nk-tb-item nk-tb-head">
                                <div class="nk-tb-col tb-col-lg"><span class="sub-text">{{ __('Email') }}</span></div>
                                @if ($pageTitle == 'Borrower')
                                    <div class="nk-tb-col tb-col-lg"><span class="sub-text">{{ __('Occupation') }}</span></div>
                                    <div class="nk-tb-col tb-col-lg"><span class="sub-text">{{ __('Age') }}</span></div>
                                @else
                                    <div class="nk-tb-col tb-col-lg"><span class="sub-text">{{ __('Type') }}</span></div>
                                @endif
                            </div>

                            {{-- User list item --}}
                            @foreach($users as $user)
                                <div class="nk-tb-item">
                                    <div class="nk-tb-col tb-col-lg">
                                        <span>{{ str_protect($user->email) }}</span>
                                    </div>
                                    @if ($pageTitle == 'Borrower')
                                        <div class="nk-tb-col tb-col-lg">
                                            <span>{{ str_protect($user->occupation->name) }}</span>
                                        </div>
                                        <div class="nk-tb-col tb-col-lg">
                                            @if ($user->age == 'less_25')
                                                <span>{{ str_protect('< 25') }}</span>
                                            @else
                                                <span>{{ str_protect($user->age) }}</span>
                                            @endif
                                        </div>
                                    @else
                                        <div class="nk-tb-col tb-col-lg">
                                            <span>{{ str_protect($user->lenderType->name) }}</span>
                                        </div>
                                    @endif
                                </div>
                            @endforeach
                        {{-- User List item-end --}}
                        </div>
                    </div>
                    {{-- Pagination --}}
                    <div class="card-inner">
                        <div class="nk-block-between-md g-3">
                            {{ $users->appends(request()->all())->links('admin.user.pagination') }}
                        </div>
                    </div>
                </div>
            </div>
            @else 
            <div class="card card-bordered text-center">
                <div class="card-inner card-inner-lg py-5">
                    @if ($pageTitle == 'Borrower')
                        <h4>{{ __("No Borrower Found") }}</h4>
                    @else
                        <h4>{{ __("No Lender Found") }}</h4>
                    @endif
                </div>
            </div>
            @endif
        </div>
    {{-- nk-block --}}
    </div>
@endsection
