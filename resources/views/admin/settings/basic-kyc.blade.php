@extends('admin.layouts.master')
@section('title', __('Components / System'))

@section('has-content-sidebar', 'has-content-sidebar')

@section('content-sidebar')
    @include('admin.settings.content-sidebar')
@endsection

@section('content')
<div class="nk-content-body">
    <div class="nk-block-head nk-block-head-sm">
        <div class="nk-block-between">
            <div class="nk-block-head-content">
                <h3 class="nk-block-title page-title">{{ __('Components') }} / {{ __("Basic KYC") }}</h3>
                <p>{{ __('Manage KYC settings to manually verify your users identity.') }}</p>
            </div>
            <div class="nk-block-head-content">
                <ul class="nk-block-tools gx-1">
                    <li class="d-lg-none">
                        <a href="#" class="btn btn-icon btn-trigger toggle" data-target="pageSidebar"><em class="icon ni ni-menu-right"></em></a>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <div class="nk-block card card-bordered">
        <div class="card-inner">
            <form class="form-settings" action="{{ route('admin.settings.component.kyc.update') }}" method="POST">
                <h5 class="title">
                    {{ __("Identity Verification") }} / {{ __("Basic KYC") }} 
                    <span class="badge badge-pill badge-xs badge-gray ml-1 nk-tooltip"{!! (is_demo()) ? ' title="This add-on is NOT part of the main package."' : '' !!}>{{ 'Module' }}</span>
                </h5>
                <p>{{ __("Allow your users to submit KYC application to verify their identity and manage applications internally.") }}</p>
                <div class="form-sets gy-3 wide-md">
                    <div class="row g-3 align-center">
                        <div class="col-md-5">
                            <div class="form-group">
                                <label class="form-label" for="kyc-feature-enable">{{ __('KYC Verification System') }}</label>
                                <span class="form-note">{{ __('Enable identity verification feature for user.') }}</span>
                            </div>
                        </div>
                        <div class="col-md-7">
                            <div class="form-group">
                                <div class="custom-control custom-switch">
                                    <input class="switch-option-value" type="hidden" name="feature_enable" value="{{ gss('kyc_feature_enable') ?? 'no' }}">
                                    <input id="kyc-feature-enable" type="checkbox" class="custom-control-input switch-option"
                                        data-switch="yes"{{ (gss('kyc_feature_enable', 'no') == 'yes') ? ' checked=""' : ''}}>
                                    <label for="kyc-feature-enable" class="custom-control-label">{{ __('Enable') }}</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="divider"></div>
                <div class="form-sets gy-3 wide-md">
                    <div class="row g-3 align-start">
                        <div class="col-md-5">
                            <div class="form-group">
                                <label class="form-label">{{ __('Identity Verification') }}</label>
                                <span class="form-note">{{ __('User allow to complete the identity verfication form.') }}</span>
                            </div>
                        </div>
                        <div class="col-md-7">
                            <div class="form-group">
                                <div class="custom-control custom-switch">
                                    <input class="switch-option-value" type="hidden" name="verification" value="{{ gss('kyc_verification') ?? 'yes' }}">
                                    <input id="kyc-verification-enable" type="checkbox" class="custom-control-input switch-option"
                                    data-switch="yes"{{ (gss('kyc_verification', 'yes') == 'yes') ? ' checked=""' : ''}}>
                                    <label for="kyc-verification-enable" class="custom-control-label">{{ __('Enable') }}</label>
                                </div>
                                <div class="form-note mt-1 text-info">{{ __("It will promote user to complete their verfication but it will be not required.") }}</div>
                            </div>
                        </div>
                    </div>
                    <div class="row g-3 align-start">
                        <div class="col-md-5">
                            <div class="form-group">
                                <label class="form-label">{{ __('Required Verification') }}</label>
                                <span class="form-note">{{ __('Before proceed, identity verification will be required.') }}</span>
                            </div>
                        </div>
                        <div class="col-md-7">
                            <div class="form-group">
                                <div class="form-control-wrap">
                                    <ul class="custom-control-group gx-1 gy-3">
                                        <li>
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input checkbox-option" id="required-kyc-withdraw" name="verified[withdraw]"{{ (gss('kyc_verified') !== null && data_get(gss('kyc_verified'), 'withdraw') == 'on') ? ' checked' : '' }}>
                                                <label for="required-kyc-withdraw" class="custom-control-label">{{ __('Withdraw') }}</label>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input checkbox-option" id="required-kyc-deposit" name="verified[deposit]"{{ (gss('kyc_verified') !== null && data_get(gss('kyc_verified'), 'deposit') == 'on') ? ' checked' : '' }}>
                                                <label for="required-kyc-deposit" class="custom-control-label">{{ __('Deposit') }}</label>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="form-note text-danger">{{ __("User unable to proceed until verified their identity.") }}</div>
                            </div>
                        </div>
                    </div>
                    <div class="row g-3 align-center">
                        <div class="col-md-5">
                            <div class="form-group">
                                <label class="form-label" for="kyc-profile-lock">{{ __('Locked Profile Information') }}</label>
                                <span class="form-note">{{ __('Profile data will be locked once submitted the document.') }}</span>
                            </div>
                        </div>
                        <div class="col-md-7">
                            <div class="form-group">
                                <div class="custom-control custom-switch">
                                    <input class="switch-option-value" type="hidden" name="profile_locked" value="{{ gss('kyc_profile_locked') ?? 'yes' }}">
                                    <input id="kyc-profile-lock" type="checkbox" class="custom-control-input switch-option"
                                    data-switch="yes"{{ (gss('kyc_profile_locked', 'yes') == 'yes') ? ' checked=""' : ''}}>
                                    <label for="kyc-profile-lock" class="custom-control-label">{{ __('Locked') }}</label>
                                </div>
                                <div class="form-note mt-1 text-info">{{ __("User will not allow to change the name, address, country, date of birth, gender etc.") }}</div>
                            </div>
                        </div>
                    </div>
                    <div class="row g-3 align-center">
                        <div class="col-md-5">
                            <div class="form-group">
                                <label class="form-label" for="kyc-complete-profile">{{ __('Complete Before Application') }}</label>
                                <span class="form-note">{{ __('User must complete the profile before submit the document.') }}</span>
                            </div>
                        </div>
                        <div class="col-md-7">
                            <div class="form-group">
                                <div class="custom-control custom-switch">
                                    <input class="switch-option-value" type="hidden" name="profile_complete" value="{{ gss('kyc_profile_complete') ?? 'yes' }}">
                                    <input id="kyc-complete-profile" type="checkbox" class="custom-control-input switch-option"
                                        data-switch="yes"{{ (gss('kyc_profile_complete', 'yes') == 'yes') ? ' checked=""' : ''}}>
                                    <label for="kyc-complete-profile" class="custom-control-label">{{ __('Enable') }}</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row g-3 align-start">
                        <div class="col-md-5">
                            <div class="form-group">
                                <label class="form-label">{{ __("Disable New Submission") }}</label>
                                <span class="form-note">{{ __("Temporarily disable new application submission.") }}</span>
                            </div>
                        </div>
                        <div class="col-md-7">
                            <div class="form-group">
                                <div class="custom-control custom-switch">
                                    <input class="switch-option-value" type="hidden" name="disable_request" value="{{ sys_settings('kyc_disable_request') ?? 'no' }}">
                                    <input id="kyc-app-disable" type="checkbox" class="custom-control-input switch-option" data-switch="yes"{{ (sys_settings('kyc_disable_request', 'no') == 'yes') ? ' checked=""' : ''}}>
                                    <label for="kyc-app-disable" class="custom-control-label">{{ __("Disable") }}</label>
                                </div>
                                <span class="form-note mt-1"><em class="text-danger">{{ __("Users unable to submit form for identity verification.") }}</em></span>
                            </div>
                        </div>
                    </div>
                    <div class="row g-3 align-start">
                        <div class="col-md-5">
                            <div class="form-group">
                                <label class="form-label">{{ __("Display Notice to User") }}</label>
                                <span class="form-note">{{ __("Add custom message to show on user-end.") }}</span>
                            </div>
                        </div>
                        <div class="col-md-7">
                            <div class="form-group">
                                <div class="form-control-wrap">
                                    <input type="text" class="form-control" name="disable_title" value="{{ sys_settings('kyc_disable_title', 'Temporarily unavailable!') }}">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="form-control-wrap">
                                    <textarea class="form-control textarea-sm" name="disable_notice">{{ sys_settings('kyc_disable_notice') }}</textarea>
                                </div>
                                <div class="form-note">
                                    <span>{{ __("This message will display when user going to proceed for identity verification.") }}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-sets gy-3 wide-md">
                    <div class="row g-3">
                        <div class="col-md-7 offset-lg-5">
                            <div class="form-group mt-2">
                                @csrf
                                <input type="hidden" name="form_type" value="kyc-settings">
                                <button type="button" class="btn btn-primary submit-settings" disabled="">
                                    <span class="spinner-border spinner-border-sm hide" role="status" aria-hidden="true"></span>
                                    <span>{{ __('Update') }}</span>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div class="nk-block card card-bordered">
        <div class="card-inner">
            <form class="form-settings" action="{{ route('admin.settings.component.kyc.update') }}" method="POST">
                <h5 class="title">{{ __('KYC Form Settings') }}</h5>
                <p>{{ __("Manage your KYC application form to submit documents.") }}</p>
                <div class="form-set gy-3 wide-md">
                    <div class="row g-3 align-center">
                        <div class="col-md-5">
                            <div class="form-group">
                                <label class="form-label" for="kyc-profile-preview">{{ __('Preview Without Asking') }}</label>
                                <span class="form-note">{{ __('Form will not display if already present the user information.') }}</span>
                            </div>
                        </div>
                        <div class="col-md-7">
                            <div class="form-group">
                                <div class="custom-control custom-switch">
                                    <input class="switch-option-value" type="hidden" name="preview_quick" value="{{ gss('kyc_preview_quick') ?? 'yes' }}">
                                    <input id="kyc-profile-preview" type="checkbox" class="custom-control-input switch-option"
                                        data-switch="yes"{{ (gss('kyc_preview_quick', 'yes') == 'yes') ? ' checked=""' : ''}}>
                                    <label for="kyc-profile-preview" class="custom-control-label">{{ __('Enable') }}</label>
                                </div>
                                <div class="form-note mt-1 text-info">
                                    {{ __("User allow update the info before submission.") }}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row g-3 align-start">
                        <div class="col-md-5">
                            <div class="form-group">
                                <label class="form-label">{{ __("Fields for User Information") }}</label>
                                <span class="form-note">{{ __("Select whatever you need for identity verification.") }}</span>
                            </div>
                        </div>
                        <div class="col-md-7">
                                                    <ul class="custom-control-group gx-1 gy-2 flex-wrap flex-column">
                                                            <li class="input-group w-max-350px flex-wrap flex-sm-nowrap justify-between">
                            <div class="input-label">Full Name</div>
                            <div class="input-fields">
                                <ul class="gx-gs justify-between">
                                    <li>
                                        <div class="custom-control custom-control-sm custom-switch">
                                            <input class="switch-option-value" type="hidden" name="fields[profile][name][show]" value="yes">
                                            <input id="profile-name-show" type="checkbox" class="custom-control-input switch-option" data-switch="yes" checked="" disabled="">
                                            <label for="profile-name-show" class="custom-control-label">Show</label>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="custom-control custom-control-sm custom-checkbox">
                                            <input class="switch-option-value" type="hidden" name="fields[profile][name][req]" value="yes">
                                            <input id="profile-name-req" type="checkbox" class="custom-control-input switch-option" data-switch="yes" checked="" disabled="">
                                            <label for="profile-name-req" class="custom-control-label">Required</label>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </li>
                                                            <li class="input-group w-max-350px flex-wrap flex-sm-nowrap justify-between">
                            <div class="input-label">Date of Birth</div>
                            <div class="input-fields">
                                <ul class="gx-gs justify-between">
                                    <li>
                                        <div class="custom-control custom-control-sm custom-switch">
                                            <input class="switch-option-value" type="hidden" name="fields[profile][dob][show]" value="yes">
                                            <input id="profile-dob-show" type="checkbox" class="custom-control-input switch-option" data-switch="yes" checked="">
                                            <label for="profile-dob-show" class="custom-control-label">Show</label>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="custom-control custom-control-sm custom-checkbox">
                                            <input class="switch-option-value" type="hidden" name="fields[profile][dob][req]" value="yes">
                                            <input id="profile-dob-req" type="checkbox" class="custom-control-input switch-option" data-switch="yes" checked="">
                                            <label for="profile-dob-req" class="custom-control-label">Required</label>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </li>
                                                            <li class="input-group w-max-350px flex-wrap flex-sm-nowrap justify-between">
                            <div class="input-label">Phone Number</div>
                            <div class="input-fields">
                                <ul class="gx-gs justify-between">
                                    <li>
                                        <div class="custom-control custom-control-sm custom-switch">
                                            <input class="switch-option-value" type="hidden" name="fields[profile][phone][show]" value="no">
                                            <input id="profile-phone-show" type="checkbox" class="custom-control-input switch-option" data-switch="yes">
                                            <label for="profile-phone-show" class="custom-control-label">Show</label>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="custom-control custom-control-sm custom-checkbox">
                                            <input class="switch-option-value" type="hidden" name="fields[profile][phone][req]" value="no">
                                            <input id="profile-phone-req" type="checkbox" class="custom-control-input switch-option" data-switch="yes">
                                            <label for="profile-phone-req" class="custom-control-label">Required</label>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </li>
                                                            <li class="input-group w-max-350px flex-wrap flex-sm-nowrap justify-between">
                            <div class="input-label">Gender</div>
                            <div class="input-fields">
                                <ul class="gx-gs justify-between">
                                    <li>
                                        <div class="custom-control custom-control-sm custom-switch">
                                            <input class="switch-option-value" type="hidden" name="fields[profile][gender][show]" value="no">
                                            <input id="profile-gender-show" type="checkbox" class="custom-control-input switch-option" data-switch="yes">
                                            <label for="profile-gender-show" class="custom-control-label">Show</label>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="custom-control custom-control-sm custom-checkbox">
                                            <input class="switch-option-value" type="hidden" name="fields[profile][gender][req]" value="no">
                                            <input id="profile-gender-req" type="checkbox" class="custom-control-input switch-option" data-switch="yes">
                                            <label for="profile-gender-req" class="custom-control-label">Required</label>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </li>
                                                            <li class="input-group w-max-350px flex-wrap flex-sm-nowrap justify-between">
                            <div class="input-label">Nationality</div>
                            <div class="input-fields">
                                <ul class="gx-gs justify-between">
                                    <li>
                                        <div class="custom-control custom-control-sm custom-switch">
                                            <input class="switch-option-value" type="hidden" name="fields[profile][nationality][show]" value="yes">
                                            <input id="profile-nationality-show" type="checkbox" class="custom-control-input switch-option" data-switch="yes" checked="">
                                            <label for="profile-nationality-show" class="custom-control-label">Show</label>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="custom-control custom-control-sm custom-checkbox">
                                            <input class="switch-option-value" type="hidden" name="fields[profile][nationality][req]" value="no">
                                            <input id="profile-nationality-req" type="checkbox" class="custom-control-input switch-option" data-switch="yes">
                                            <label for="profile-nationality-req" class="custom-control-label">Required</label>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </li>
                                                            <li class="input-group w-max-350px flex-wrap flex-sm-nowrap justify-between">
                            <div class="input-label">Country</div>
                            <div class="input-fields">
                                <ul class="gx-gs justify-between">
                                    <li>
                                        <div class="custom-control custom-control-sm custom-switch">
                                            <input class="switch-option-value" type="hidden" name="fields[profile][country][show]" value="yes">
                                            <input id="profile-country-show" type="checkbox" class="custom-control-input switch-option" data-switch="yes" checked="" disabled="">
                                            <label for="profile-country-show" class="custom-control-label">Show</label>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="custom-control custom-control-sm custom-checkbox">
                                            <input class="switch-option-value" type="hidden" name="fields[profile][country][req]" value="yes">
                                            <input id="profile-country-req" type="checkbox" class="custom-control-input switch-option" data-switch="yes" checked="" disabled="">
                                            <label for="profile-country-req" class="custom-control-label">Required</label>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </li>
                                                            <li class="input-group w-max-350px flex-wrap flex-sm-nowrap justify-between">
                            <div class="input-label">Address</div>
                            <div class="input-fields">
                                <ul class="gx-gs justify-between">
                                    <li>
                                        <div class="custom-control custom-control-sm custom-switch">
                                            <input class="switch-option-value" type="hidden" name="fields[profile][address][show]" value="yes">
                                            <input id="profile-address-show" type="checkbox" class="custom-control-input switch-option" data-switch="yes" checked="">
                                            <label for="profile-address-show" class="custom-control-label">Show</label>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="custom-control custom-control-sm custom-checkbox">
                                            <input class="switch-option-value" type="hidden" name="fields[profile][address][req]" value="no">
                                            <input id="profile-address-req" type="checkbox" class="custom-control-input switch-option" data-switch="yes">
                                            <label for="profile-address-req" class="custom-control-label">Required</label>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </li>
                                                        </ul>
                                                </div>
                    </div>
                </div>
                <div class="divider"></div>
                <div class="form-set gy-3 wide-md">
                    <div class="row g-3 align-start">
                        <div class="col-md-5">
                            <div class="form-group">
                                <label class="form-label">{{ __("Document for Verification") }}</label>
                                <span class="form-note">{{ __("Select document that user allow to upload for verification.") }}</span>
                            </div>
                        </div>
                        <div class="col-md-7">
                                                                <ul class="custom-control-group gx-1 gy-3 flex-wrap flex-column">
                                                                        <li>
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input checkbox-option" id="docs-main-pp-show" name="docs[main][pp]" checked="">
                                            <label for="docs-main-pp-show" class="custom-control-label">Passport</label>
                                        </div>
                                        <div class="form-note mt-1">
                                            Allow passport for user identity verification.
                                        </div>
                                    </li>
                                                                        <li>
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input checkbox-option" id="docs-main-nid-show" name="docs[main][nid]" checked="">
                                            <label for="docs-main-nid-show" class="custom-control-label">National ID</label>
                                        </div>
                                        <div class="form-note mt-1">
                                            Allow national ID for user identity verification.
                                        </div>
                                    </li>
                                                                        <li>
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input checkbox-option" id="docs-main-dvl-show" name="docs[main][dvl]">
                                            <label for="docs-main-dvl-show" class="custom-control-label">Driving License</label>
                                        </div>
                                        <div class="form-note mt-1">
                                            Allow driving license for user identity verification.
                                        </div>
                                    </li>
                                                                    </ul>
                                                                <div class="form-note text-danger mt-2">Note: User can upload only one document if multiple selected.</div>

                                <div class="form-group mt-3">
                                    <label class="form-label">More options for document</label>
                                    <div class="form-control-wrap">
                                        <ul class="custom-control-group gx-1 gy-2 flex-wrap flex-column">
                                            <li class="input-group w-max-350px flex-wrap flex-sm-nowrap justify-between">
                                                <div class="input-label">Issued by Country</div>
                                                <div class="input-fields">
                                                    <ul class="gx-gs justify-between">
                                                        <li>
                                                            <div class="custom-control custom-control-sm custom-switch">
                                                                <input type="hidden" class="switch-option-value" name="docs[field][country][show]" value="on">
                                                                <input id="docs-field-country-show" type="checkbox" class="custom-control-input switch-option" checked="" disabled="">
                                                                <label for="docs-field-country-show" class="custom-control-label">Show</label>
                                                            </div>
                                                        </li>
                                                        <li>
                                                            <div class="custom-control custom-control-sm custom-checkbox">
                                                                <input type="hidden" class="switch-option-value" name="docs[field][country][req]" value="on">
                                                                <input id="docs-field-country-req" type="checkbox" class="custom-control-input switch-option" name="docs[field][country][req]" checked="" disabled="">
                                                                <label for="docs-field-country-req" class="custom-control-label">Required</label>
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </li>
                                            <li class="input-group w-max-350px flex-wrap flex-sm-nowrap justify-between">
                                                <div class="input-label">ID Number</div>
                                                <div class="input-fields">
                                                    <ul class="gx-gs justify-between">
                                                        <li>
                                                            <div class="custom-control custom-control-sm custom-switch">
                                                                <input id="docs-field-id-show" type="checkbox" class="custom-control-input switch-option" name="docs[field][id][show]">
                                                                <label for="docs-field-id-show" class="custom-control-label">Show</label>
                                                            </div>
                                                        </li>
                                                        <li>
                                                            <div class="custom-control custom-control-sm custom-checkbox">
                                                                <input id="docs-field-id-req" type="checkbox" class="custom-control-input switch-option" name="docs[field][id][req]">
                                                                <label for="docs-field-id-req" class="custom-control-label">Required</label>
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </li>
                                            <li class="input-group w-max-350px flex-wrap flex-sm-nowrap justify-between">
                                                <div class="input-label">Issue Date</div>
                                                <div class="input-fields">
                                                    <ul class="gx-gs justify-between">
                                                        <li>
                                                            <div class="custom-control custom-control-sm custom-switch">
                                                                <input id="docs-field-issue-show" type="checkbox" class="custom-control-input switch-option" name="docs[field][issue][show]">
                                                                <label for="docs-field-issue-show" class="custom-control-label">Show</label>
                                                            </div>
                                                        </li>
                                                        <li>
                                                            <div class="custom-control custom-control-sm custom-checkbox">
                                                                <input id="docs-field-issue-req" type="checkbox" class="custom-control-input switch-option" name="docs[field][issue][req]">
                                                                <label for="docs-field-issue-req" class="custom-control-label">Required</label>
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </li>
                                            <li class="input-group w-max-350px flex-wrap flex-sm-nowrap justify-between">
                                                <div class="input-label">Expiry Date</div>
                                                <div class="input-fields">
                                                    <ul class="gx-gs justify-between">
                                                        <li>
                                                            <div class="custom-control custom-control-sm custom-switch">
                                                                <input id="docs-field-expiry-show" type="checkbox" class="custom-control-input switch-option" name="docs[field][expiry][show]">
                                                                <label for="docs-field-expiry-show" class="custom-control-label">Show</label>
                                                            </div>
                                                        </li>
                                                        <li>
                                                            <div class="custom-control custom-control-sm custom-checkbox">
                                                                <input id="docs-field-expiry-req" type="checkbox" class="custom-control-input switch-option" name="docs[field][expiry][req]">
                                                                <label for="docs-field-expiry-req" class="custom-control-label">Required</label>
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                    <span class="form-note">User allow to input their document information.</span>
                                </div>

                            </div>
                    </div>

                    <div class="row g-3 align-start">
                        <div class="col-md-5">
                            <div class="form-group">
                                <label class="form-label">{{ __("Upload Selfie for Proof") }}</label>
                                <span class="form-note">{{ __("Select if user need to upload selfie for proof.") }}</span>
                            </div>
                        </div>
                        <div class="col-md-7">
                            <div class="form-group">
                                <div class="custom-control custom-switch">
                                    <input class="switch-option-value" type="hidden" name="doc_selfie" value="{{ gss('kyc_doc_selfie') ?? 'yes' }}">
                                    <input id="kyc-doc-selfie" type="checkbox" class="custom-control-input switch-option"
                                        data-switch="yes"{{ (gss('kyc_doc_selfie', 'yes') == 'yes') ? ' checked=""' : ''}}>
                                    <label for="kyc-doc-selfie" class="custom-control-label">{{ __('Enable') }}</label>
                                </div>
                                <div class="form-note mt-1 text-info">
                                    {{ __("If Enable, Selfie with document will be required.") }}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="divider"></div>
                <div class="form-sets gy-3 wide-md">
                    <div class="row g-3 align-start">
                        <div class="col-md-5">
                            <div class="form-group">
                                <label class="form-label">{{ __("Additional Documents") }}</label>
                                <span class="form-note">{{ __("Select if you need additionally for verification.") }}</span>
                            </div>
                        </div>
                        <div class="col-md-7">
                                                                <ul class="custom-control-group gx-1 gy-3 flex-wrap flex-column">
                                                                        <li>
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input checkbox-option" id="docs-alter-bs-show" name="docs[alter][bs]" checked="">
                                            <label for="docs-alter-bs-show" class="custom-control-label">Bank Statement</label>
                                        </div>
                                        <div class="form-note mt-1">
                                            Allow bank statements for additional verification.
                                        </div>
                                    </li>
                                                                        <li>
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input checkbox-option" id="docs-alter-ub-show" name="docs[alter][ub]">
                                            <label for="docs-alter-ub-show" class="custom-control-label">Utility Bill</label>
                                        </div>
                                        <div class="form-note mt-1">
                                            Allow utility bill for additional verification.
                                        </div>
                                    </li>
                                                                    </ul>
                                                                <div class="form-group mt-3">
                                    <label class="form-label">More options for document</label>
                                    <div class="form-control-wrap">
                                        <div class="custom-control custom-control-sm custom-switch">
                                            <input type="checkbox" class="custom-control-input switch-option" id="docs-alter-required-opt" name="docs[alter][required]">
                                            <label for="docs-alter-required-opt" class="custom-control-label">Both Require to Upload</label>
                                        </div>
                                    </div>
                                    <div class="form-note text-danger">Note: By default one will be required if both enabled.</div>
                                </div>
                            </div>
                    </div>
                </div>

                <div class="form-sets gy-3 wide-md">
                    <div class="row g-3">
                        <div class="col-md-7 offset-lg-5">
                            <div class="form-group mt-2">
                                @csrf
                                <input type="hidden" name="form_type" value="kyc-form-settings">
                                <button type="button" class="btn btn-primary submit-settings" disabled="">
                                    <span class="spinner-border spinner-border-sm hide" role="status" aria-hidden="true"></span>
                                    <span>{{ __('Update') }}</span>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
