<div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">

                    <h4 class="modal-title">Attention</h4>
                </div>
                <div class="modal-body">
                    <p>Please enter security OTP to continue. Press "Send Email" to start sending OTP to your email.</p>
                    <button class="btn btn-block btn-primary" data-toggle="modal" data-target="#modalOtp">Insert OTP</button>
                </div>
            </div>
        </div>