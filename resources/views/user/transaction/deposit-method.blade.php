@extends('user.layouts.master')

@section('title', __('Deposit Funds'))

@section('content')
<div class="nk-content-body">
    <div class="page-dw wide-xs m-auto" id="pms-ajcon">
        @if (!empty($errors) && is_array($errors))
        @include('user.transaction.error-state', $errors)
        @else
        <div class="nk-pps-apps">
            <div class="nk-pps-steps">
                <span class="step active step_1"></span>
                <span class="step step_2"></span>
                <span class="step step_3"></span>
                <span class="step step_4"></span>
            </div>
            <div class="nk-pps-title text-center">
                <h3 class="title">{{ __('Deposit Funds') }}</h3>
                <p class="caption-text">{{ __('We are currently only accepting USDT and USDC via TRON TRC-20 network')
                    }}</p>
                <p class="sub-text-sm">{{ __('') }}</p>
            </div>
            <form class="nk-pps-form form-validate is-alter" action="{{ route('deposit.amount.form') }}"
                id="dpst-pm-frm" data-required_msg="{{ __('To deposit, please select a payment method.') }}">
                <div class="nk-pps-field form-group" id="form-method">
                    <ul class="nk-pm-list" id="payment-option-list">
                        @foreach($activeMethods as $item)
                            <li class="nk-pm-item">
                                <input class="nk-pm-control" type="radio" name="deposit_method" required
                                    value="{{ data_get($item, 'slug') }}" id="{{ data_get($item, 'slug') }}" />
                                <label class="nk-pm-label" for="{{ data_get($item, 'slug') }}">
                                    <span class="pm-name">{{ __(data_get($item, 'name')) == 'Xanpool' ? 'PayNow via XanPool' : __(data_get($item, 'name'))  }}</span>
                                    <span class="pm-icon"><em
                                            class="icon ni {{ data_get($item, 'module_config.icon') }}"></em></span>
                                </label>
                            </li>
                        @endforeach
                    </ul>
                </div>
                <div class="nk-pps-field form-action text-center">
                    <div class="nk-pps-action" id="btn-nonxanpool">
                        <a href="javascript:void(0)" class="btn btn-lg btn-block btn-primary deposit-now" id="deposit-now">
                            <span>{{ __('Deposit Now') }}</span>
                            <span class="spinner-border spinner-border-sm hide" role="status" aria-hidden="true"></span>
                        </a>
                    </div>
                </div>
            </form>
        </div>
        @endif
    </div>
    @if (!empty($errors) && is_array($errors))
    @else
        <div class="page-dw wide-xs mx-auto mt-5">
            @foreach($activeMethods as $item)
                <button class="collapsible">{{ __(data_get($item, 'name')) == 'Xanpool' ? 'How to deposit via PayNow via XanPool' : 'How to deposit via '.$item->name  }}</button>
                <div class="content">
                    @if( __(data_get($item, 'name')) != 'Xanpool')
                    <div class="mt-3 mx-3">
                        <ol>
                            <li class="py-2">
                                Select “Crypto Wallet” and click on “Deposit Now”
                                <img class="img-fluid mt-3" src="{{asset('images/crypto/c1.png')}}">
                            </li>
                            <li class="py-2">
                                Input the amount of USDT you want to deposit and click on “Continue to Deposit”
                                <img class="img-fluid mt-3" src="{{asset('images/crypto/c2.png')}}">
                            </li>
                            <li class="py-2">
                                Review your deposit and click on “Confirm & Pay"
                                <img class="img-fluid mt-3" src="{{asset('images/crypto/c3.png')}}">
                            </li>
                            <li class="py-2">
                                Send the amount to our USDT-TRC20 address (THHcCyLHHhZK57NxbeUC2tqmjcxmevmnr2). Once done, click on “I have transferred to the Tether wallet”.
                                <img class="img-fluid mt-3" src="{{asset('images/crypto/c4.png')}}">
                            </li>
                            <li class="py-2">
                                Enter your email address in the field and click on “Payment Confirmed”
                                <img class="img-fluid mt-3" src="{{asset('images/crypto/c5.png')}}">
                            </li>
                            <li class="py-2">That’s it! We will process and confirm your deposit within 24 hours and after that, you should be able to see an updated account balance.</li>
                        </ol>
                    </div>
                    @else
                    <div class="mt-3 mx-3">
                        <ol>
                            <li class="py-2">
                                Select “PayNow via XanPool” and click on “Deposit Now”
                                <img class="img-fluid mt-3" src="{{asset('images/xanpool/x1.png')}}">
                            </li>
                            <li class="py-2">
                                Input the amount of USDT you want to deposit and click on “Buy USDT”
                                <img class="img-fluid mt-3" src="{{asset('images/xanpool/x2.png')}}">
                            </li>
                            <li class="py-2">
                                Login to your XanPool account, or create an account if you haven’t already.
                                <img class="img-fluid mt-3" src="{{asset('images/xanpool/x3.png')}}">
                            </li>
                            <li class="py-2">
                                Click on “Switch to Tron”
                                <img class="img-fluid mt-3" src="{{asset('images/xanpool/x4.png')}}">
                            </li>
                            <li class="py-2">
                                Type in our Tron address (THHcCyLHHhZK57NxbeUC2tqmjcxmevmnr2) and click on “Continue”
                                <img class="img-fluid mt-3" src="{{asset('images/xanpool/x5.png')}}">
                            </li>
                            <li class="py-2">
                                Review your order and click on “Proceed to Payment”. Please note that the amount credited into your account will be “USDT To Receive”. In this example, that value is 492.5 USDT.
                                <img class="img-fluid mt-3" src="{{asset('images/xanpool/x6.png')}}">
                            </li>
                            <li class="py-2">
                                Using the payment instructions, make your payment via PayNow. Once done, click on “I have made payment”.
                                <img class="img-fluid mt-3" src="{{asset('images/xanpool/x7.png')}}">
                            </li>
                            <li class="py-2">
                                Click on “Confirm & Pay”. The amount
                                <img class="img-fluid mt-3" src="{{asset('images/xanpool/x8.png')}}">
                            </li>
                            <li class="py-2">That’s it! We will process and confirm your deposit within 24 hours and after that, you should be able to see an updated account balance.</li>
                        </ol>
                    </div>
                    @endif
                </div>
            @endforeach
        </div>
    @endif
</div>
@endsection
@section('custom_js')
<style type="text/css">
    ol{
        list-style: decimal;
    }
    .widgetContainer {
        width: 400px !important;
        margin: auto;
    }
    .collapsible {
  background-color: #777;
  color: white;
  cursor: pointer;
  padding: 18px;
  width: 100%;
  border: none;
  text-align: left;
  outline: none;
  font-size: 15px;
}

.active, .collapsible:hover {
  background-color: #555;
}

.collapsible:after {
  content: '\002B';
  color: white;
  font-weight: bold;
  float: right;
  margin-left: 5px;
}

.active:after {
  content: "\2212";
}

.content {
  padding: 0 18px;
  max-height: 0;
  overflow: hidden;
  transition: max-height 0.2s ease-out;
  background-color: #fff;
}
</style>

<script>
var coll = document.getElementsByClassName("collapsible");
var i;

for (i = 0; i < coll.length; i++) {
  coll[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var content = this.nextElementSibling;
    if (content.style.maxHeight){
      content.style.maxHeight = null;
    } else {
      content.style.maxHeight = content.scrollHeight + "px";
    } 
  });
}
</script>
@endsection