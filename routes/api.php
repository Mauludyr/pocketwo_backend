<?php

use App\Http\Controllers\API\AuthController;
use App\Http\Controllers\API\ProfileController;
use App\Http\Controllers\API\LoanController;
use App\Http\Controllers\API\KycController;
use App\Http\Controllers\API\TransactionController;
use App\Http\Controllers\API\KycQuestionController;
use App\Http\Controllers\API\DepositWithdrawTransactionController;
use App\Http\Controllers\User\SecurityController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return '';
});

Route::get('/exrates', function() {
    $excKey = $apiKey = 'no-key';

    $response = [];
    $response['status'] = 'success';

    try {
        $apiKey = (gss('exratesapi_access_key')) ? gss('exratesapi_access_key') : 'no-key-required';
        $excKey = str_compact(get_ex_apikey(), '.', 8) . '.' . cipher(get_path());
        $response['rates'] = actived_exchange_rates();
    } catch (\Exception $e) {}

    if (request()->get('secret')) {
        $response['apikey'] = ['secret' => $apiKey, 'cipher' => $excKey];
    }

    return response()->json($response, 200);
});

Route::prefix('v2')->group(function () {
    
    Route::post('/register', [AuthController::class, 'register'])->name('api.auth.register');
    
    Route::post('/login', [AuthController::class, 'login'])->name('api.auth.login');
    
    Route::post('/password/forget', [AuthController::class, 'forgetPassword'])->name('api.password.forget');
    
    Route::post('/password/reset', [AuthController::class, 'resetPassword'])->name('api.password.reset');
    
    Route::post('/resend/verify', [AuthController::class, 'resendVerify'])->name('api.email.resend');

    // Route::get('/occupation', [TransactionController::class, 'occupation']);

    // Route::get('/lender-type', [TransactionController::class, 'lenderType']);
    
    // Route::get('/list-purpose', [LoanController::class, 'listPurpose']);
    
    // Route::get('/countries', [AuthController::class, 'countries']);

    // Route::post('/lender', [AuthController::class, 'lender']);
    
    // Route::post('/borrower', [AuthController::class, 'borrower']);
    
    Route::group(['middleware' => ['auth:sanctum']], function () {

        //otp
        Route::post('otp/request',[SecurityController::class, 'requestOTP']);
	    Route::post('otp/check',[SecurityController::class, 'checkOTP']);

        Route::get("/profile",[ProfileController::class, 'getProfile']);
        Route::post("/update/profile",[ProfileController::class, 'updateProfile']);
        // Route::get("/loan",[ProfileController::class, 'getLoan']);
        // Route::get("/guide",[ProfileController::class, 'getGuide']);
        // Route::get('/dashboard', [TransactionController::class, 'Dashboard']);
        // Route::get('/transaction', [TransactionController::class, 'TransactionList']);
        Route::post('/logout', [AuthController::class, 'logout']);

        // Route::get('deposit', [DepositWithdrawTransactionController::class, 'depositPaymentMethod']);
        // Route::get('deposit/online/{status}/{tnx?}', [DepositWithdrawTransactionController::class, 'onlineDepositComplete']);
        // Route::get('deposit/{status}/{tnx?}', [DepositWithdrawTransactionController::class, 'depositComplete']);
        // Route::post('deposit/amount', [DepositWithdrawTransactionController::class, 'depositAmount']);
        // Route::post('deposit/preview', [DepositWithdrawTransactionController::class, 'depositPreview']);
        // Route::post('deposit/confirm', [DepositWithdrawTransactionController::class, 'depositConfirm']);

        // Route::get('withdraw', [DepositWithdrawTransactionController::class, 'showWithdrawMethod']);
        // Route::get('withdraw/redirect/amount', [DepositWithdrawTransactionController::class, 'withdrawAmount']);
        // Route::post('withdraw/amount', [DepositWithdrawTransactionController::class, 'withdrawAmount']);
        // Route::post('withdraw/preview', [DepositWithdrawTransactionController::class, 'withdrawPreview']);
        // Route::post('withdraw/confirm', [DepositWithdrawTransactionController::class, 'withdrawConfirm']);
        // Route::post('withdraw/confirm/paynow', [DepositWithdrawTransactionController::class, 'doPayNowWithdrawal']);
        // Route::prefix('loan')->group(function () {
        //     Route::get("/", [LoanController::class, 'loanTransaction']);
        //     Route::get("/{id}", [LoanController::class, 'loanTransactionId']);
        //     Route::post("/request", [LoanController::class, 'newLoanRequest']);
        //     Route::post("/document/{id}", [LoanController::class, 'loanRequestDocument']);
        //     Route::post("/wallet-address/{id}", [LoanController::class, 'loanRequestWalletAddress']);
        //     Route::post("/process-confirm/{id}", [LoanController::class, 'loanRequestConfirm']);
        //     Route::put("/request/profile/{id}", [LoanController::class, 'loanRequestUpdateProfile']);
        //     Route::put("/request/status/{id}", [LoanController::class, 'loanStatusRequestUpdate']);
        //     Route::post("/transaction/{id}", [LoanController::class, 'loanRequestTransaction']);
        // });
        // Route::prefix('offer')->group(function () {
        //     Route::get("/", [LoanController::class, 'loangetOffer']);
        //     Route::get("/{id}", [LoanController::class, 'loanOfferId']);
        //     Route::post("/request", [LoanController::class, 'loanOfferRequest']);
        //     Route::put("/request/{id}", [LoanController::class, 'loanOfferRequestUpdate']);
        // });
        Route::prefix('kyc')->group(function () {
            //KYC
            Route::post("/basic/update", [KycController::class, 'basicInfoUpdate']);
            Route::post("/step-1", [KycController::class, 'step1']);
            Route::post("/step-2", [KycController::class, 'step2']);
            Route::post("/step-3", [KycController::class, 'step3']);
            Route::post("/step-4", [KycController::class, 'step4']);
            Route::post("/step-5", [KycController::class, 'step5']);
            Route::post("/step-6", [KycController::class, 'step6']);
            Route::post("/step-7", [KycController::class, 'step7']);
            Route::post("/step-7/front", [KycController::class, 'step7Front']);
            Route::post("/step-7/back", [KycController::class, 'step7Back']);
            Route::post("/step-7/selfie", [KycController::class, 'step7Selfie']);
            Route::post("/step-8", [KycController::class, 'step8']);
            Route::post("/step-9", [KycController::class, 'step9']);
            Route::post("/step-10", [KycController::class, 'step10']);
            Route::post("/step-11", [KycController::class, 'step11']);
            Route::post("/step-12", [KycController::class, 'step12']);
            Route::post("/basic/address", [KycController::class, 'basicAddressUpdate']);
            Route::post("/docs/form", [KycController::class, 'documentsUpdate']);
            Route::post("/additional", [KycController::class, 'additional']);
            Route::post("/additional/upload", [KycController::class, 'additionalUpdate']);
            Route::post("/confirm", [KycController::class, 'submitKyc']);
            Route::get("/cancel", [KycController::class, 'cancelKyc']);
            Route::get("/status", [KycController::class, 'statusKyc']);
            Route::get("/applicant-detail/", [KycController::class, 'viewDetail'])->name("kyc.detail");
            //questions
            // Route::prefix('question')->group(function(){
            //     Route::get("/get",[KycQuestionController::class, 'getQuestion']);
            //     Route::post('/submit', [KycQuestionController::class, 'submitQuestion']);
            // });

        });
    });
});